﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MinecraftServerLuncherWebsite.Data
{    
    /// <summary>
    /// 
    /// </summary>
    public class Download
    {
        [Required]
        public string Version { get; set; }
        [Required]        
        public string Description { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Released { get; set; }
        [Required]
        [DataType(DataType.Url)]
        public string Software { get; set; }
        [Required]
        [DataType(DataType.Url)]
        public string Source { get; set; }
        public bool IsStable { get; set; }
    }
}
