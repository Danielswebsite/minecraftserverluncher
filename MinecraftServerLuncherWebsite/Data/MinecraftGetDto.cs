﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MinecraftServerLuncherWebsite.Data
{
    /// <summary>
    /// Data tranfer object for minecraft.
    /// </summary>
    public class MinecraftGetDto
    {        
        public DateTime Date { get; set; }        
        public string Version { get; set; }
    }
}
