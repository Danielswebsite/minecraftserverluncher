﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MinecraftServerLuncherWebsite.Data
{
    public class DownloadService
    {
        /// <summary>
        /// Download the last pushed version from Gitlab.
        /// </summary>
        public void DownloadLastPush() 
        {
        
        }

        /// <summary>
        /// Get downloads.
        /// </summary>
        /// <returns></returns>
        public ICollection<Download> GetDownloads()
        {
            DirectoryInfo dir = new DirectoryInfo($@"{Environment.CurrentDirectory}\wwwroot\downloads");

            ICollection<Download> downloads = new List<Download>();

            FileInfo downloadFileInfo;

            JsonSerializer jsonSerializer = JsonSerializer.Create();

            foreach (DirectoryInfo subdir in dir.GetDirectories())
            {
                downloadFileInfo = subdir.GetFiles().FirstOrDefault(i => i.Name.Equals("downloadinfo.json"));

                using (FileStream fileStream = File.OpenRead(downloadFileInfo.FullName))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream))
                    {
                        downloads.Add(JsonConvert.DeserializeObject<Download>(streamReader.ReadToEnd()));
                    }
                }
            }

            return downloads;
        }

    }

}
