﻿using Microsoft.AspNetCore.Mvc;
using MinecraftServerLuncherWebsite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

// https://www.hanselman.com/blog/ASPNETCoreRESTfulWebAPIVersioningMadeEasy.aspx
namespace MinecraftServerLuncherWebsite.Controllers
{
    /// <summary>
    /// Minecraft api controller to get infomation aboute minecraft.
    /// </summary>
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class MinecraftController : ControllerBase
    {
        /// <summary>
        /// Get all infomation aboute the minecrafts versions and releases.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces(MediaTypeNames.Application.Json)]
        public IEnumerable<MinecraftGetDto> Get()
        {
            // This only test data for now!
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.13.0" };
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.13.1" };
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.13.2" };
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.14.0" };
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.14.1" };
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.14.2" };
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.15.0" };
            yield return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.15.1" };
        }

        [HttpGet("{minecraftversion}")]
        [Produces(MediaTypeNames.Application.Json)]
        public MinecraftGetDto Get(string minecraftversion)
        {
            return new MinecraftGetDto() { Date = DateTime.Now, Version = "1.15.1" };
        }

    }
}
