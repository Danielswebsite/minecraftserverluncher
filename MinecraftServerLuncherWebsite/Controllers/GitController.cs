﻿using Microsoft.AspNetCore.Mvc;
using MinecraftServerLuncherWebsite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

/// <summary>
/// https://wellsb.com/csharp/aspnet/blazor-httpclientfactory-and-web-api/
/// </summary>
namespace MinecraftServerLuncherWebsite.Controllers
{
    /// <summary>
    /// Api controller.
    /// </summary>
    [ApiController]    
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public sealed class GitController : ControllerBase
    {

        /// <summary>
        /// Constructor there take the database context for to instaciate it.
        /// </summary>
        public GitController()
        {
            
        }

        /// <summary>
        /// Post for git controller.
        /// Save data from git webhuck request with new push. 
        /// </summary>
        /// <returns></returns>
        [HttpPost]                
        public async Task<IActionResult> Post([FromBody]Download download)
        {            
            // Check for is token from git webhuck are valid.
            if (IsGitTokenValid())
            {
                // Check for is download properties data is valid,
                if (ModelState.IsValid)
                {                   
                    return StatusCode((int) HttpStatusCode.Accepted,"Data is set with success.");
                }
                return BadRequest(ModelState);
            }
            return StatusCode((int) HttpStatusCode.Unauthorized, "Git access token is not valid.");
        }

        /// <summary>
        /// Check for is token from git webhuck are valid.
        /// </summary>
        /// <returns></returns>
        private bool IsGitTokenValid() 
        {
            return Request.Headers["X-Gitlab-Token"].Equals("qw6dfQWD2344w9ef749w8e4w9e87fewr98gear+7aRA+ER8G79ER+98H7G+E8qwdH7T7R+9H");
        }

    }
}
