﻿using Microsoft.AspNetCore.Mvc;
using MinecraftServerLuncherWebsite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MinecraftServerLuncherWebsite.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class MinecraftServerLauncherController : ControllerBase
    {

        /// <summary>
        /// Get all versions of the minecarft server lucancher.
        /// </summary>
        /// <returns></returns>
        public ICollection<Download> Get() 
        {
            DownloadService downloadService = new DownloadService();
            return downloadService.GetDownloads();
        }

    }
}
