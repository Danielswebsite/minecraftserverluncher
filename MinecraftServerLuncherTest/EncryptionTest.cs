﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSF.Network.Security;

namespace MinecraftServerLuncherTest
{
    
    [TestClass]
    public class EncryptionTest
    {
        
        [DataTestMethod]
        [DataRow("Skinke", "#HyggeHejSa122!", "qwdqwd4w9e87ewr9f79qwe79q&we2")]        
        public void EncryptAndDescryptTest(string text, string password, string salt)
        {
            HostEncryption hostEncryption = new HostEncryption(password, salt);
            byte[] bytes = hostEncryption.EncryptBytes(Encoding.ASCII.GetBytes(text));           
            Assert.AreNotEqual(text, Encoding.ASCII.GetString(bytes));
            bytes = hostEncryption.DecryptBytes(bytes);            
            Assert.AreEqual(text, Encoding.ASCII.GetString(bytes));
        }
    }
}
