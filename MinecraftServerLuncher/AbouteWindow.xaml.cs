﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MinecraftServerLuncher.ViewModels;

namespace MinecraftServerLuncher
{
    /// <summary>
    /// Interaction logic for AbouteWindow.xaml
    /// </summary>
    public partial class AbouteWindow : Window
    {
        public AbouteWindow()
        {
            DataContext = new AboutViewModel();
            InitializeComponent();
        }
    }
}
