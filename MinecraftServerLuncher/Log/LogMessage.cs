﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Log
{
    public class LogMessage
    {        
        public enum Code
        {
            Exception,
            Success,
            Warning,
            log
        }

        public string Description { get; set; }
        public Code LogCode { get; set; }
    }
}
