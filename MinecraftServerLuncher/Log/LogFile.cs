﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Log
{
    public static class LogFile
    {
        private static string path => $"{ Environment.CurrentDirectory }\\log";
        
        public static void AddLog(LogMessage message)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string filename = $"log_{DateTime.Now.ToString("dd-MM-yyyy")}.log";
            File.AppendAllText($"{ path }\\{ filename }", $"[{ DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") }][{ message.LogCode }] { message.Description }{Environment.NewLine}");
        }

        public static void AddLog(Exception exception)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string filename = $"log_{DateTime.Now.ToString("dd-MM-yyyy")}.log";

            StackTrace stack = new StackTrace(exception, true);
            StackFrame frame = stack.GetFrame(0);
            string message = $"{exception.GetType().Name} thrown in in { frame.GetFileName() } on line {frame.GetFileLineNumber()}.";
            File.AppendAllText($"{ path }\\{ filename }", $"[{ DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss") }][{ exception.GetType().Name }] { message }{Environment.NewLine}");
        }

    }
}
