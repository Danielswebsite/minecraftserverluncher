﻿using MSF.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using MinecraftServerLuncher.Models;
using MinecraftServerLuncher.Storege;
using System.ServiceProcess;
using MinecraftServerLuncher.ViewModels;
using System.Threading;
using MinecraftServerLuncher.Log;
using MSF.Network;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using MinecraftServerLuncher.Handlers;
using System.Windows.Controls;
using MSF.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSF.Network.Packet;
using MSF.Network.Security;

namespace MinecraftServerLuncher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    ///     
    public partial class App : Application
    {

        public static HostClient Client { get; private set; }

        public static HostUser User { get; set; }

        [STAThread]        
        public static void Main(string[] args)
        {            
            WindowHandler.HideMainWindowWithClose = true;
            ProcessHandler.OpenCurrentProcessAsSingelInstance();
            App app = new App();

            Client = new HostClient();

            app.Initialize();
            app.Notify();
            app.Run();
        }

        private void Notify()
        {
            System.Windows.Forms.NotifyIcon notify = new System.Windows.Forms.NotifyIcon();
            notify.Icon = Icon.ExtractAssociatedIcon("MinecraftServerLuncher.exe");
            notify.Visible = true;
            notify.Text = "Minecraft Server Luncher";
            notify.Click += On_Event;
        }

        private void On_Event(object sender, EventArgs e)
        {
            WindowHandler<MinecraftServerLuncherWindow> window = new WindowHandler<MinecraftServerLuncherWindow>();
            window.OpenWindow();
        }

        private void Initialize()
        {
            SplashScreen splash = new SplashScreen(@"Access\SplashScreen.png");
            splash.Show(true);
            Thread.Sleep(3000);
            StartupUri = new Uri("MinecraftServerLuncherWindow.xaml", System.UriKind.Relative);
        }
    }
}
