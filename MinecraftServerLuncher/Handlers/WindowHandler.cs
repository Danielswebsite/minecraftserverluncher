﻿using MinecraftServerLuncher.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftServerLuncher.Handlers
{
    internal partial class WindowHandler
    {
        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindow(string lp1, string lp2);

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        public enum User32WindowHandle
        {
            SW_SHOW = 5,
            SW_RESTORE = 9,
            SW_SHOWDEFAULT = 10
        }

        public static bool AskBeforClose { get; set; } = true;
        public static bool HideMainWindowWithClose { get; set; }
        public static void SetMainWindow(Window mainwindow)
        {
            Application.Current.MainWindow = mainwindow;
        }
        public static Window MainWindow { get { return Application.Current.MainWindow; } }

    }

    internal partial class WindowHandler<T> where T : Window
    {
        private string askbefore_title;

        private string askbefore_message;

        private T window;

        private List<T> windows => Application.Current.Windows.OfType<T>().ToList();

        public WindowHandler()
        {
            if (windows.Exists(i => i.GetType() == typeof(T)))
                window = windows.FirstOrDefault(i => i.GetType() == typeof(T));
            else
                window = Activator.CreateInstance<T>();

            window.Closing += Close_Event;
        }

        private WindowHandler(int index)
        {
            if (windows.Count > 0)
                window = windows[index];
            else
                window = Activator.CreateInstance<T>();

            window.Activated += OnActivated_Event;
            window.Closing += Close_Event;
        }

        private void Close_Event(object sender, CancelEventArgs e)
        {
            var window = (T)sender;

            if (askbefore_title != null && askbefore_message != null && WindowHandler.AskBeforClose && !WindowHandler.HideMainWindowWithClose)
            {
                MessageBoxResult result = MessageBox.Show(askbefore_message, askbefore_title, MessageBoxButton.YesNo);
                e.Cancel = (result == MessageBoxResult.No);
            }

            if (WindowHandler.HideMainWindowWithClose)
            {
                if (window == WindowHandler.MainWindow)
                {
                    window.Hide();
                    e.Cancel = true;
                }
            }
        }

        private void Close_OpenOtherWindow_Event<W>(object sender, CancelEventArgs e) where W : Window
        {
            WindowHandler<W> wh = WindowHandler<W>.GetHandler(0);
            wh.OpenWindow();
        }

        public void AskBeforClose(string title, string message)
        {
            askbefore_title = title;
            askbefore_message = message;
        }

        public static WindowHandler<T> GetHandler(int index)
        {
            return new WindowHandler<T>(index);
        }

        private static void OnActivated_Event(object sender, EventArgs e)
        {
            var window = (Window)sender;
            window.Show();
        }

        public void SetState(WindowState state)
        {
            window.WindowState = state;
        }

        public void SetToMainWindow()
        {
            Application.Current.MainWindow = window;
        }

        public WindowState State
        {
            get => window.WindowState;

            set
            {
                window.WindowState = value;
            }
        }

        public void OpenWindow()
        {
            window.Show();
        }

        public void OpenWindowAsDialog()
        {
            window.Topmost = true;
            window.ShowDialog();
        }

        public void OpenWindowAsDialog(ViewModel viewmodel)
        {
            window.Topmost = true;
            window.DataContext = viewmodel;
            window.ShowDialog();
        }

        public void GoToWindowOnWindowClose<W>() where W : Window
        {
            window.Closing += Close_OpenOtherWindow_Event<W>;
        }

        public void SwitchOpenWindow<W>(int index) where W : Window
        {
            WindowHandler.AskBeforClose = false;
            OpenWindow();
            WindowHandler<W>.GetHandler(index).CloseWindow();
            WindowHandler.AskBeforClose = true;
        }

        public void OpenWindow(ViewModel viewmodel)
        {
            window.DataContext = viewmodel;
            window.Show();
        }

        public void CloseWindow()
        {
            window.Close();
        }
    }
}
