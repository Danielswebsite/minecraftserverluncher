﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftServerLuncher.Handlers
{
    internal class ProcessHandler
    {
        private static string processname = "MinecraftServerLuncher";

        public static void OpenCurrentProcessAsSingelInstance()
        {
            var currentprocessor = Process.GetProcessesByName(processname);

            if (currentprocessor.Length > 1)
            {
                try
                {
                    IntPtr w = WindowHandler.FindWindow(null, "Minecraft Server Luncher");

                    if (w != IntPtr.Zero)
                    {
                        WindowHandler.ShowWindow(w, (int) WindowHandler.User32WindowHandle.SW_RESTORE);
                        WindowHandler.ShowWindow(w, (int) WindowHandler.User32WindowHandle.SW_SHOW);
                        WindowHandler.SetForegroundWindow(w);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                Environment.Exit(0);
            }
        }
    }
}
