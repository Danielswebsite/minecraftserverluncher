﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Minecraft.Player
{
    public class MinecraftPlayer
    {
        public string Uuid { get; set; }
        public string Name { get; set; }
    }
}
