﻿using MinecraftServerLuncher.Minecraft.Java;
using MinecraftServerLuncher.Minecraft.Player;
using MinecraftServerLuncher.Minecraft.Plugin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftServerLuncher.Minecraft.Server
{
    public sealed class MinecraftServer
    {
        public MinecraftServer()
        {
            JavaServices = new JavaServices();
        }

        private string proppath => $"{ServerPath}/server.properties";

        public int MinMemery { get; set; }

        public JavaServices JavaServices { get; private set; }

        public int MaxMemery { get; set; }

        public string TerminalOutput { get; private set; }

        public string GetServerFolderName()
        {
            return ServerPath.Split('\\').Last();
        }

        public string SetServerFolderName(string name)
        {
            try
            {
                string newfolderpath = ServerPath.Replace(GetServerFolderName(), name);

                if (!Directory.Exists(newfolderpath))
                {
                    Directory.Move(ServerPath, newfolderpath);
                    ServerPath = newfolderpath;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return ServerPath;
        }

        public List<MinecraftPlugin> GetPlugins()
        {
            string path = $"{ ServerPath }\\plugins";

            var plugins = new List<MinecraftPlugin>();

            try
            {
                if (Directory.Exists(path))
                {
                    foreach (var pluginpath in Directory.GetFiles(path))
                    {
                        plugins.Add(new MinecraftPlugin() { PluginFolderPath = path, PluginPath = pluginpath });
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return plugins;
        }

        public List<MinecraftPlayer> GetWhiteListPlayers()
        {
            var players = new List<MinecraftPlayer>();

            string path = $"{ ServerPath }\\whitelist.json";

            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader reader = File.OpenText(path))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        players = (List<MinecraftPlayer>)serializer.Deserialize(reader, typeof(List<MinecraftPlayer>));
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return players;
        }

        public void AddPlayerToWhiteList(MinecraftPlayer player)
        {
            try
            {
                var players = GetWhiteListPlayers();
                players.Add(player);
                var json = JsonConvert.SerializeObject(players, Formatting.Indented);
                string path = $"{ ServerPath }\\whitelist.json";
                File.WriteAllText(path, json);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void RemovePlayerFromWhiteList(MinecraftPlayer player)
        {
            try
            {
                var players = GetWhiteListPlayers();

                players.Remove(players.Find(i => i.Name == player.Name && i.Uuid == player.Uuid));

                string path = $"{ ServerPath }\\whitelist.json";

                var json = JsonConvert.SerializeObject(players, Formatting.Indented);

                File.WriteAllText(path, json);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        public event FileSystemEventHandler ServerPropertiesChangeEvent;

        private void OnServerPropertiesChange(object sender, FileSystemEventArgs e)
        {
            ServerPropertiesChangeEvent?.Invoke(sender, e);
        }

        public void Remove()
        {
            if (Directory.Exists(ServerPath))
            {
                try
                {
                    var dir = new DirectoryInfo(ServerPath);
                    dir.Attributes = FileAttributes.Normal;
                    dir.Delete(true);
                }
                catch (Exception e)
                {
                    MessageBox.Show($"Error: { e.ToString() }", "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);

                }
            }
        }

        public void WatchForPropertiesChanges()
        {
            FileSystemWatcher watcher = new FileSystemWatcher();

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
       | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            watcher.Filter = "server.properties";
            watcher.Path = ServerPath;
            watcher.Changed += OnServerPropertiesChange;
            watcher.Created += OnServerPropertiesChange;
            watcher.EnableRaisingEvents = true;
        }

        public void RemovePlugin(MinecraftPlugin plugin)
        {
            try
            {
                File.Delete(plugin.PluginPath);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), $"Error Exception:", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public MinecraftPlugin AddPlugin(string path)
        {
            var plugin = new MinecraftPlugin();

            string pluginpath = $"{ServerPath}\\plugins\\{path.Split('\\').Last()}";

            try
            {
                File.Copy(path, pluginpath);
                plugin.PluginFolderPath = pluginpath;
                plugin.PluginPath = $"{pluginpath}\\{path.Split('\\').Last()}";
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return plugin;
        }

        public string GetServerFileName()
        {
            try
            {
                return Directory.GetFiles(ServerPath).First(i => i.Contains(".jar"));
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return string.Empty;
        }

        public string ServerPath { get; set; }

        public bool HasProperties()
        {
            if (!File.Exists(proppath))
                return false;

            int numberoflines = File.ReadAllLines(proppath).Length;
            return (numberoflines > 3);
        }

        public MinecraftServerProperties GetProperties()
        {
            MinecraftServerProperties properties = new MinecraftServerProperties();

            if (HasProperties())
                try
                {
                    string workcopyproppath = $"{ proppath.Split('.')[0] }_workcopy.{ proppath.Split('.')[1] }";

                    if (File.Exists(workcopyproppath))
                        File.Delete(workcopyproppath);

                    File.Copy(proppath, workcopyproppath);
                    var lines = File.ReadAllLines(workcopyproppath);
                    File.Delete(workcopyproppath);
                    for (int i = 0; i < lines.Length; i++)
                    {
                        string[] line = lines[i].Split('=');

                        if (line[0].Equals("spawn-protection"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnProtection = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("max-tick-time"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxTickTime = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("generator-settings"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.GeneratorSettings = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("force-gamemode"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ForceGamemode = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("allow-nether"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.AllowNether = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("gamemode"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Gamemode = byte.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("broadcast-console-to-ops"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.BroadcastConsoleToOps = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("enable-query"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.EnableQuery = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("player-idle-timeout"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.PlayerIdleTimeout = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("difficulty"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Difficulty = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("spawn-monsters"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnMonsters = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("op-permission-level"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.OpPermissionLevel = byte.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("pvp"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Pvp = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("snooper-enabled"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SnooperEnabled = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("level-type"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.LevelType = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("hardcore"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Hardcore = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("enable-command-block"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.EnableCommandBlock = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("max-players"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxPlayers = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("network-compression-threshold"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.NetworkCompressionThreshold = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("resource-pack-sha1"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ResourcePackSha1 = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("max-world-size"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxWorldSize = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("server-port"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ServerPort = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("server-ip"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ServerIp = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("spawn-npcs"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnNpcs = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("allow-flight"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.AllowFlight = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("level-name"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.LevelName = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("view-distance"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ViewDistance = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("resource-pack"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ResourcePack = line[1];
                                }

                            }
                        }

                        if (line[0].Equals("spawn-animals"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnAnimals = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("white-list"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.WhiteList = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("generate-structures"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.GenerateStructures = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("online-mode"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.OnlineMode = bool.Parse(line[1]);
                                }

                            }
                        }

                        if (line[0].Equals("max-build-height"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxBuildHeight = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("level-seed"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.LevelSeed = long.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("prevent-proxy-connections"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.PreventProxyConnections = bool.Parse(line[1]);
                                }

                            }
                        }

                        if (line[0].Equals("enable-rcon"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.EnableRcon = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("motd"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Motd = line[1];
                                }
                            }
                        }

                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            return properties;
        }

        public void SetProperties(MinecraftServerProperties properties)
        {
            string proppath = $"{ServerPath}/server.properties";

            try
            {
                using (StreamWriter reader = new StreamWriter(proppath))
                {
                    reader.WriteLine($"max-tick-time={properties.MaxTickTime}");
                    reader.WriteLine($"generator-settings={properties.GeneratorSettings}");
                    reader.WriteLine($"force-gamemode={properties.ForceGamemode}");
                    reader.WriteLine($"allow-nether={properties.AllowNether}");
                    reader.WriteLine($"gamemode={properties.Gamemode}");
                    reader.WriteLine($"broadcast-console-to-ops={properties.BroadcastConsoleToOps}");
                    reader.WriteLine($"enable-query={properties.EnableQuery}");
                    reader.WriteLine($"player-idle-timeout={properties.PlayerIdleTimeout}");
                    reader.WriteLine($"difficulty={properties.Difficulty}");
                    reader.WriteLine($"spawn-monsters={properties.SpawnMonsters}");
                    reader.WriteLine($"pvp={properties.Pvp}");
                    reader.WriteLine($"snooper-enabled={properties.SnooperEnabled}");
                    reader.WriteLine($"level-type={properties.LevelType}");
                    reader.WriteLine($"hardcore={properties.Hardcore}");
                    reader.WriteLine($"enable-command-block={properties.EnableCommandBlock}");
                    reader.WriteLine($"max-players={properties.MaxPlayers}");
                    reader.WriteLine($"network-compression-threshold={properties.NetworkCompressionThreshold}");
                    reader.WriteLine($"resource-pack-sha1={properties.ResourcePackSha1}");
                    reader.WriteLine($"max-world-size={properties.MaxWorldSize}");
                    reader.WriteLine($"server-port={properties.ServerPort}");
                    reader.WriteLine($"server-ip={properties.ServerIp}");
                    reader.WriteLine($"spawn-npcs={properties.SpawnNpcs}");
                    reader.WriteLine($"allow-flight={properties.AllowFlight}");
                    reader.WriteLine($"level-name={properties.LevelName}");
                    reader.WriteLine($"view-distance={properties.ViewDistance}");
                    reader.WriteLine($"resource-pack={properties.ResourcePack}");
                    reader.WriteLine($"spawn-animals={properties.SpawnAnimals}");
                    reader.WriteLine($"white-list={properties.WhiteList}");
                    reader.WriteLine($"generate-structures={properties.GenerateStructures}");
                    reader.WriteLine($"online-mode={properties.OnlineMode}");
                    reader.WriteLine($"max-build-height={properties.MaxBuildHeight}");
                    reader.WriteLine($"level-seed={properties.LevelSeed}");
                    reader.WriteLine($"prevent-proxy-connections={properties.PreventProxyConnections}");
                    reader.WriteLine($"motd={properties.Motd}");
                    reader.WriteLine($"enable-rcon={properties.EnableRcon}");
                }
            }

            catch (Exception e)
            {
                MessageBox.Show($"{ e }", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public bool HasBinStartedBefore()
        {
            return !(Directory.GetDirectories(ServerPath).Length <= 1 && Directory.GetFiles(ServerPath).Length <= 3);
        }

        public void Start()
        {
            try
            {
                Thread thread = new Thread(() =>
                {
                    JavaServices.Args = new string[] { $"-Xms{ MinMemery }M", $"-Xmx{ MaxMemery }M", "-jar", $"{ GetServerFileName()}" };
                    JavaServices.WorkingFolder = ServerPath;
                    JavaServices.Start();
                });

                thread.Name = $"Start JavaServices_{GetServerFolderName()}";
                thread.Start();
            }
            catch (ThreadStartException e)
            {
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void Stop()
        {
            try
            {
                Thread thread = new Thread(() =>
                {
                    JavaServices.Stop();
                });
                thread.Name = $"Stop JavaServices_{GetServerFolderName()}";
                thread.Start();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        
    }
}
