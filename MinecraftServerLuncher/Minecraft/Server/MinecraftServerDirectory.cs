
using System;
using MinecraftServerLuncher.Minecraft.Tools;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftServerLuncher.Minecraft.Server
{
    public class MinecraftServerDirectory
    {
        private string path { get; set; }

        public MinecraftServerDirectory(string path)
        {
            this.path = path;
        }

        public ICollection<MinecraftServer> GetMinecraftServers()
        {
            var servers = new List<MinecraftServer>();

            foreach (var path in Directory.GetDirectories(path))
            {
                servers.Add(new MinecraftServer() { ServerPath = path, });
            }

            return servers;
        }
        
        public void DeleteServer(string name)
        {
            try
            {
                Directory.Delete($"{path}\\{name}", true);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
        
        public MinecraftServer CreateServer(string serverfilepath)
        {
            int i = Directory.GetDirectories(path).Length + 1;

            string serverpath = $"{path}\\NewFolder{i}";

            try
            {
                while (Directory.Exists(serverpath))
                {
                    i++;
                    serverpath = $"{path}\\NewFolder{i}";
                }

                Directory.CreateDirectory(serverpath);

                string name = serverpath.Split('\\').Last();

                File.Copy(serverfilepath, $"{ serverpath }\\{ serverfilepath.Split('\\').Last() }");
            }
            catch (Exception e)
            {
                MessageBox.Show("Error Exception", e.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return new MinecraftServer() { ServerPath = serverpath, };

        }
    }
}
