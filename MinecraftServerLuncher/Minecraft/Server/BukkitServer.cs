﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MinecraftServerLuncher.Minecraft.Plugin;

namespace MinecraftServerLuncher.Minecraft.Server.Bukkit
{
    public class BukkitServer : MinecraftServer
    {
        public override ICollection<BukkitPlugin> Plugins()
        {
            var path = $"{ ServerPath }\\plugins";

            var plugins = new List<BukkitPlugin>();

            if (!Directory.Exists(path))
                return plugins;

            var dirs = Directory.GetFiles(path);

            foreach (var dir in dirs)
            {
                if (File.Exists(dir))
                    plugins.Add(new BukkitPlugin() { PluginPath = dir, PluginFolderPath = path });
            }
            return plugins;
        }
    }
}
