﻿using MinecraftServerLuncher.Minecraft.Java;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Minecraft.Tools
{
    public class BuildTools
    {        
        private static BuildTools buildtools { get; set; }
        
        private BuildTools(string path)
        {
            this.path = path;
        }

        private BuildTools()
        {
            
        }
        
        private string path{ get; set; }

        public static BuildTools GetBuildTools(string path)
        {
            if (buildtools == null)
            {
                buildtools = new BuildTools(path);
            }
            return buildtools;
        }
        
        public static BuildTools GetBuildTools()
        {
            if (buildtools == null)
            {
                buildtools = new BuildTools();
            }
            return buildtools;
        }


        public bool Exists()
        {
            if (Directory.Exists(path))
            {
              return File.Exists($"{ path }\\BuildTools.jar");
            }
            return false;
        }


        public List<string> GetMinecraftServersVersions()
        {
            var serversversions = new List<string>();

            foreach (var filename in Directory.GetFiles(path, "*.jar").ToList().FindAll(i => !i.Contains("BuildTools.jar")))
            {
                serversversions.Add(filename.Split('\\').Last());
            }
            return serversversions;
        }

        public void DownloadBuildTools()
        {
            using (var webclient = new WebClient())
            {
                string filepath = $"{ path }\\BuildTools.jar";

                if (!File.Exists(filepath))                
                    webclient.DownloadFile("https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar", filepath);                
            }           
        }

        public JavaServices RunBuildTools(string minecrafversion)
        {          
            JavaServices services = new JavaServices();
            services.Args = new string[] { "-jar", "BuildTools.jar", "--rev", minecrafversion };
            services.WorkingFolder = path;
            services.Start();
            return services;
        }

    }
}
