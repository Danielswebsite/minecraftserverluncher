﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftServerLuncher.Minecraft.Java
{
    public class JavaServices
    {
        public Process Process { get; private set; }

        public string[] Args { get; set; }

        public string WorkingFolder { get; set; }
        
        public bool IsRunning()
        {
            var allprocess = Process.GetProcesses();

            if (Process == null)
                return false;

            return allprocess.ToList().Exists(i => i.Id == Process.Id);
        }

        private string Augumenter()
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < Args.Length; i++)
            {
                if (i != 0)
                    builder.Append($" {Args[i]}");
                else
                    builder.Append($"{Args[i]}");
            }

            return builder.ToString();
        }
        
        public void Stop()
        {
            try
            {
                Process.Kill();
                Process.Dispose();
                Process = null;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void Start()
        {
            string args = Augumenter();

            try
            {
                var startprocess = new ProcessStartInfo("java.exe", args);
                startprocess.CreateNoWindow = true;
                startprocess.UseShellExecute = false;
                startprocess.WorkingDirectory = WorkingFolder;
                startprocess.RedirectStandardError = true;
                startprocess.RedirectStandardOutput = true;

                if ((Process = Process.Start(startprocess)) == null)
                {
                    throw new InvalidOperationException("MinecraftServer java file not exits!");
                }

                Process.EnableRaisingEvents = true;
                Process.BeginOutputReadLine();                
                Process.WaitForExit();
                Process = null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
