﻿using Microsoft.Win32;
using MinecraftServerLuncher.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MinecraftServerLuncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MinecraftServerLuncherWindow : Window
    {
        public MinecraftServerLuncherWindow()
        {
            var viewmodel = new ServerLuncherViewModel();
            DataContext = viewmodel;
            InitializeComponent();            
        }
    }
}
