﻿using MinecraftServerLuncher.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MinecraftServerLuncher.Commands
{
    /// <summary>
    /// Window command for open and close the windows by this types.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class WindowCommand<T> : ICommand where T : Window
    {
        /// <summary>
        /// Change event for can execute command or not. 
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// WindowHandler type there has exstended from window class.
        /// </summary>
        WindowHandler<T> window;

        /// <summary>
        /// Enum property there representate the action for the window.
        /// </summary>
        private WindowAction action;

        /// <summary>
        /// Enum there representate the action for the window.
        /// </summary>
        public enum WindowAction
        {
            Close,
            Open,
            ToogleSize,
            Minisize
        }

        /// <summary>
        /// Window command there take a for handle the window.
        /// </summary>
        /// <param name="action"></param>
        public WindowCommand(WindowAction action)
        {            
            this.action = action;
        }

        /// <summary>
        /// Can execute. This will all time retine true.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Execute the command for the window handled by action enmum.
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            window = new WindowHandler<T>();

            switch (action)
            {
                case WindowAction.Close:
                    window.CloseWindow();
                    break;
                case WindowAction.Open:
                    window.OpenWindow();
                    break;
                case WindowAction.Minisize:
                    window.State = WindowState.Minimized;
                    break;

                case WindowAction.ToogleSize:

                    if (window.State == WindowState.Maximized)
                        window.State = WindowState.Normal;
                    else
                        window.State = WindowState.Maximized;
                    break;
            }
        }
    }
}
