﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MinecraftServerLuncher.Models;

namespace MinecraftServerLuncher.Commands
{
    /// <summary>
    /// Command for buttons and other WPF controls there can execute commands.
    /// </summary>
    public class Command : ICommand
    {

        /// <summary>
        /// Action or method to be executet by the command on click from button or another WPF controll there can use command.
        /// </summary>
        protected Action<object> ExecuteAction;
       
        /// <summary>
        /// Disable the command so the action and button or another WPF control will not execute of be enablet in the UI.
        /// </summary>
        public bool DiableCommand { protected get; set; }

        /// <summary>
        /// Command there tage a action/method to be executet by a button or another WPF controll. 
        /// </summary>
        /// <param name="executemethod"></param>
        public Command(Action<object> executemethod)
        {
            this.ExecuteAction = executemethod;
        }

        /// <summary>
        /// Eventhandler for the command action can execute or not.
        /// </summary>
        public event EventHandler CanExecuteChanged = delegate { };
        
        /// <summary>
        /// On the can execute or not has change it.
        /// </summary>
        protected void OnExecute()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Execute the action from the construktor.
        /// </summary>
        void ICommand.Execute(object parameter)
        {
            if (ExecuteAction != null)
            {
                ExecuteAction(parameter);
            }
        }

        /// <summary>
        /// Can execute the command action or not.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public virtual bool CanExecute(object parameter)
        {
            if (ExecuteAction != null)
                if(!DiableCommand)
                return true;

            return false;           
        }
    }
}
