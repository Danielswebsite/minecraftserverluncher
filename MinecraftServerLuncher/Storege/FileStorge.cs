﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MinecraftServerLuncher.Storege
{
    public abstract class FileStorge
    {
        protected string Path { get; set; }

        public FileStorge(string path)
        {
            Path = path;
        }

        public delegate void FileChangeHandler(DateTime lastchange, string path);

        private bool run;

        private bool firstloop = true;

        public event FileChangeHandler FileChangeEvent;

        protected DateTime LastChange { get; set; }

        protected void OnChange(DateTime lastchange, string path)
        {
            FileChangeEvent?.Invoke(lastchange, path);
        }

        public async void StartChangeListnerAsync()
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                DateTime lastchangetime;
                run = true;

                while (run)
                {
                    lastchangetime = File.GetLastAccessTime(Path);

                    if (!firstloop)
                    {
                        if (lastchangetime != LastChange)
                        {
                            OnChange(lastchangetime, Path);
                            while (true)
                            {
                                lastchangetime = File.GetLastAccessTime(Path);
                                if (lastchangetime == LastChange)
                                {
                                    break;
                                }
                                LastChange = lastchangetime;
                            }
                        }
                    }
                    else
                    {
                        firstloop = false;
                    }
                    LastChange = lastchangetime;
                }
            });
        }

        public void Stop()
        {
            run = false;
        }
    }
}
