﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Storege
{
    public class DatStorge : FileStorge
    {
        public DatStorge(string path) : base(path)
        {

        }

        public void Save(string data)
        {
            using (FileStream fstream = File.OpenWrite($"{Environment.CurrentDirectory}\\{ Path }"))
            {
                byte[] bytes = Encoding.ASCII.GetBytes(data);
                fstream.Write(bytes, 0, bytes.Length);
            }
        }

        public string Load()
        {
            using (FileStream fileStream = File.OpenRead($"{Environment.CurrentDirectory}\\{ Path }"))
            {
                using (StreamReader streamReader = new StreamReader(fileStream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

    }
}
