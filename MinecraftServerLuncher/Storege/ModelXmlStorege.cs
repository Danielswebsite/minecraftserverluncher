﻿using MinecraftServerLuncher.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MinecraftServerLuncher.Storege
{
    public class ModelXmlStorege<T> : FileStorge
    {
        public ModelXmlStorege(string Path) : base(Path)
        {

        }

        public delegate void ModelXmlStoregeHandler(object sender, EventArgs e);

        public event ModelXmlStoregeHandler SaveEvent;

        public event ModelXmlStoregeHandler LoadEvent;

        protected void OnSave(object sender, EventArgs e)
        {
            SaveEvent?.Invoke(sender, e);
        }

        protected void OnLoad(object sender, EventArgs e)
        {
            LoadEvent?.Invoke(sender, e);
        }

        public string Namespace { private get; set; }

        public string Prefix { private get; set; }

        private T readxml(FileStream file)
        {
            using (StreamReader stream = new StreamReader(file))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stream);
            }
        }

        private T writexml(FileStream file, T model)
        {
            using (StreamWriter stream = new StreamWriter(file))
            {
                XmlSerializer serializer = new XmlSerializer(model.GetType());

                if (string.Empty != Namespace && string.Empty != Prefix)
                {
                    XmlSerializerNamespaces xmlnamepace = new XmlSerializerNamespaces();
                    xmlnamepace.Add(Prefix, Namespace);
                    serializer.Serialize(stream, model, xmlnamepace);
                }
                else
                {
                    serializer.Serialize(stream, model);
                }

                return model;
            }
        }

        public T LoadModel()
        {
            if (File.Exists(Path))
            {
                using (FileStream file = File.OpenRead(Path))
                {
                    T model = readxml(file);
                    OnLoad(model, new EventArgs());
                    return model;
                }
            }

            else
            {
                using (FileStream file = File.Create(Path))
                {
                    return writexml(file, (T)Activator.CreateInstance(typeof(T)));
                }
            }
        }

        public void SaveModel(T model)
        {
            using (FileStream file = File.Create(Path))
            {
                writexml(file, model);
                OnSave(model, new EventArgs());
            }
        }

    }
}
