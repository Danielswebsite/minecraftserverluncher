﻿using MinecraftServerLuncher.Exceptions;
using MinecraftServerLuncher.Models;
using MinecraftServerLuncher.Storege;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace MinecraftServerLuncher.ViewModels
{
    public abstract class ViewModel : INotifyPropertyChanged
    {
        
        protected ModelXmlStorege<OptionsModel> Storege;

        private object commandparameter;
        public virtual object CommandParameter
        {
            get
            {
                return commandparameter; 
            }
            set
            {
                commandparameter = value;
                OnPropertyChanged("CommandParameter");
            }
        }

        public ViewModel()
        {
            try
            {
                Storege = new ModelXmlStorege<OptionsModel>(@".\Options.xml");
                Storege.Namespace = "http://minecraft-server-launcher.com/options";
                Storege.Prefix = "mslop";                
            }
            catch (Exception e)
            {
                throw new MinecraftServerLuncherException(e.Message);
            }
        }

        private ICollection<Window> Windows => Application.Current.Windows.OfType<Window>().ToList();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }

        protected BitmapImage LoadBitmapImage(string path)
        {
            BitmapImage image = new BitmapImage();

            if (File.Exists(path))
            {
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = new Uri(path);
                image.EndInit();
            }

            return image;
        }

        protected SolidColorBrush GetBrushByHexCode(string hex)
        {
            SolidColorBrush brush = null;
            Application.Current.Dispatcher.Invoke(() =>
            {
                brush = (SolidColorBrush)new BrushConverter().ConvertFromString(hex);
            });
            return brush;
        }

        protected void TextToSpeek(string text)
        {
            new Thread(() =>
            {
                SpeechSynthesizer speech = new SpeechSynthesizer();
                speech.Volume = 100;
                speech.Rate = -2;
                speech.SpeakAsync(text);
            }).Start();
        }
    }
}
