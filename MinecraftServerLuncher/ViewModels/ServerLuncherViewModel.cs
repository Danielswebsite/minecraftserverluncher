﻿using MinecraftServerLuncher.Commands;
using MinecraftServerLuncher.Log;
using MSF.Server;
using MSF.Tools;
using MinecraftServerLuncher.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Microsoft.Win32;
using MSF.Player;
using MinecraftServerLuncher.Exceptions;
using MinecraftServerLuncher.Handlers;
using MSF.Network;
using System.Globalization;
using System.Collections.Specialized;
using MSF.Network.Minecarft;

namespace MinecraftServerLuncher.ViewModels
{
    public class ServerLuncherViewModel : ViewModel
    {
        public override object CommandParameter { get => base.CommandParameter; set => base.CommandParameter = value; }

        private ServerModel server;

        public ServerModel Server
        {
            get
            {
                return server;
            }
            set
            {
                server = value;
                OnPropertyChanged("Server");
            }
        }

        private MinecraftServerDirectory mcdir;

        public OptionsModel OptionsModel;

        public PlayerModel Player { get; set; }

        public ObservableCollection<ServerModel> Servers { get; set; }

        public PathModel Path { get; set; }

        public ObservableCollection<ServerVersionModel> ServerVersions { get; set; }

        private ServerVersionModel serverversionmodel;

        public ServerVersionModel ServerVersionModel
        {
            get
            {
                return serverversionmodel;
            }
            set
            {
                serverversionmodel = value;
                OnPropertyChanged("ServerVersionModel");
            }

        }

        public Command AddServerCommand { get; set; }

        public Command ChangeWhitelistCommand { get; set; }

        public Command RemoveServerCommand { get; set; }

        public Command ShowLogCommand { get; set; }

        public Command AddPluginCommand { get; set; }

        public Command OpenAddPlayerWindowCommand { get; set; }

        public Command RemovePlayerFromServer { get; set; }

        public Command AddPluginToServerCommand { get; set; }

        public Command StartAndStopServerCommand { get; set; }

        public Command OpenBuildToolsCommand { get; set; }

        public Command AddPlayerToWhitelistCommand { get; set; }

        public Command AddPlayerToOperatorsCommand { get; set; }

        public Command RemovePluginCommand { get; set; }

        public Command DeOpPlayerCommand { get; set; }

        public Command CloseAddPluginWindowCommand { get; set; }

        public Command OpenOptionsWindowCommand { get; set; }

        public Command CloseAddPlayerToWhiteListWindowCommand { get; set; }

        public Command CloseAddOpWindowsCommand { get; set; }

        public Command CloseAppCommand { get; set; }

        public Command OpenAbouteWindowCommand { get; set; }

        public Command OpenConectWindowCommand { get; set; }

        public WindowCommand<MinecraftServerLuncherWindow> CloseWindowCommand { get; set; }
        public WindowCommand<MinecraftServerLuncherWindow> ToogleWindowCommand { get; set; }
        public WindowCommand<MinecraftServerLuncherWindow> MinisizeWindowCommand { get; set; }
        public WindowCommand<AddPlayerWhitelistWindow> CloseAddPlayerToWhiteListCommand { get; set; }
        public WindowCommand<AddPlayerToOperatorWindow> CloseAddPlayerToOperatorsCommand { get; set; }
        private MinecraftServerHostInformationProvider ServerHostInformationProvider { get; set; }

        public Command OpenIconWindowCommand { get; set; }

        public ServerLuncherViewModel()
        {
            try
            {
                OptionsModel = Storege.LoadModel();
                if (OptionsModel.MinecraftServersPath == null || OptionsModel.BuildToolsPath == null)
                {
                    OptionsModel.AutoOpdateBuildTools = false;
                    OptionsModel.BuildToolsPath = "c:\\BuildTools";
                    OptionsModel.LastUpdate = DateTime.Now;
                    OptionsModel.UpdateEveryDay = false;
                    OptionsModel.UpdateEveryMonth = false;
                    OptionsModel.UpdateEveryWeek = false;
                    OptionsModel.MinecraftServersPath = "c:\\MinecraftServers";
                    OptionsModel.TargetMiencraftVersion = new ServerVersionModel() { Version = "1.10.2" };
                    Storege.SaveModel(OptionsModel);
                }
            }
            catch (Exception e)
            {
                throw new MinecraftServerLuncherException(e.Message);
            }

            Storege.FileChangeEvent += (date, path) =>
            {
                Application.Current.Dispatcher.Invoke(async () =>
                {
                    foreach (var srv in Servers)
                        if (srv.Status == ServerModel.EnableStatus.Running || srv.Status == ServerModel.EnableStatus.Starting)
                            if (srv.MinecraftServer is MinecraftServer)
                                await ((MinecraftServer)srv.MinecraftServer).StopAsync(true).ConfigureAwait(true);

                    OptionsModel = Storege.LoadModel();

                    mcdir = new MinecraftServerDirectory(OptionsModel.MinecraftServersPath);

                    try
                    {
                        PropertyChanged -= PropertyChanged_Event;
                        loadservers();
                        loadbuildtools();
                    }
                    catch (Exception e)
                    {
                        LogFile.AddLog(e);
                        MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                });
            };

            Storege.StartChangeListnerAsync();

            mcdir = new MinecraftServerDirectory(OptionsModel.MinecraftServersPath);

            Path = new PathModel();
            Player = new PlayerModel();
            CloseAddPlayerToWhiteListWindowCommand = new Command(closeaddplayertowhitelistwindow);
            AddServerCommand = new Command(addserver);
            RemoveServerCommand = new Command(removeserver);
            AddPluginCommand = new Command(addplugin);
            OpenBuildToolsCommand = new Command(openbuildtoolswindow);
            RemovePluginCommand = new Command(removeplugin);
            AddPlayerToWhitelistCommand = new Command(addplayertowhitelist);
            AddPlayerToOperatorsCommand = new Command(addplayertooperator);
            OpenAddPlayerWindowCommand = new Command(openaddplayerwindow);
            DeOpPlayerCommand = new Command(deopplayer);
            CloseAppCommand = new Command(closeapp);
            OpenAbouteWindowCommand = new Command(openaboutewindow);
            RemovePlayerFromServer = new Command(removeplayerfromwhitelist);
            ChangeWhitelistCommand = new Command(changewhitelist);
            OpenOptionsWindowCommand = new Command(openoptionswindow);
            StartAndStopServerCommand = new Command(startandstopserver);
            ShowLogCommand = new Command(showlog);
            OpenIconWindowCommand = new Command(openiconwindow);
            CloseAddOpWindowsCommand = new Command(closeaddopwindow);

            OpenConectWindowCommand = new Command(OpenConnectWindow);

            CloseAddPlayerToOperatorsCommand = new WindowCommand<AddPlayerToOperatorWindow>(WindowCommand<AddPlayerToOperatorWindow>.WindowAction.Close);
            CloseAddPlayerToWhiteListCommand = new WindowCommand<AddPlayerWhitelistWindow>(WindowCommand<AddPlayerWhitelistWindow>.WindowAction.Close);

            MinisizeWindowCommand = new WindowCommand<MinecraftServerLuncherWindow>(WindowCommand<MinecraftServerLuncherWindow>.WindowAction.Minisize);
            CloseWindowCommand = new WindowCommand<MinecraftServerLuncherWindow>(WindowCommand<MinecraftServerLuncherWindow>.WindowAction.Close);
            ToogleWindowCommand = new WindowCommand<MinecraftServerLuncherWindow>(WindowCommand<MinecraftServerLuncherWindow>.WindowAction.ToogleSize);

            try
            {
                loadservers();
                loadbuildtools();
            }
            catch (Exception e)
            {
                LogFile.AddLog(e);
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            WindowHandler<MinecraftServerLuncherWindow> whandler = WindowHandler<MinecraftServerLuncherWindow>.GetHandler(0);
            whandler.AskBeforClose("Luk", "Sikker på du ønsker at lukke programmet?");

            autoupdatebuildtools();

            PropertyChanged += PropertyChanged_Event;
            autoselect();

            App.Client.ConnectedEvent += ClientConnect_Event;
        }

        private void ClientConnect_Event(object sender, HostEventArgs e)
        {
            Task.Run(() =>
            {
                // TODO: Make better solution for this.
                while (App.User is null);
                ServerHostInformationProvider = new MinecraftServerHostInformationProvider(App.Client, App.User.Token);
                ServerHostInformationProvider.ResiveServerEvent += ServerResive_Event;
                ServerHostInformationProvider.RequestServerList();
            });
        }

        /// <summary>
        /// Resive event for hostserver.
        /// </summary>
        private void ServerResive_Event(object sender, HostResiveServerEventArgs e)
        {
            Application.Current.Dispatcher?.Invoke(() =>
            {
                MinecraftHostServer server = e.HostServer;

                var servermodel = new ServerModel();
                servermodel.AllowFligth = true;
                servermodel.EnableCommandBlock = false;
                servermodel.Host = server.Host;
                servermodel.Port = server.Port;
                servermodel.MOTD = "A minecraft server";
                servermodel.PVP = true;
                servermodel.SpawnNPCs = true;
                servermodel.SpawnAnimals = true;
                servermodel.EnableProperties = true;
                servermodel.MaxPlayers = 20;
                servermodel.Name = server.Name;
                servermodel.StartAndStop = new StartAndStopModel() { Text = "Start", Brushes = GetBrushByHexCode("#384a00") };

                servermodel.OnlineMode = true;
                servermodel.PropertyChanged += PropertyChanged_Event;
                servermodel.MinecraftServer = e.HostServer;

                servermodel.FilePath = server.FilePath;
                servermodel.Plugins = new ObservableCollection<PluginModel>();
                servermodel.Players = new ObservableCollection<PlayerModel>();
                servermodel.Ops = new ObservableCollection<PlayerModel>();
                servermodel.Ops.CollectionChanged += PropertyChanged_Event;
                servermodel.Players.CollectionChanged += PropertyChanged_Event;
                servermodel.Icon = LoadBitmapImage("");

                Servers.Add(servermodel);

            });

        }

        private void autoupdatebuildtools()
        {
            BuildTools buildTools = BuildTools.GetBuildTools();

            if (OptionsModel.UpdateEveryDay)
            {
                if (OptionsModel.LastUpdate.ToString("yyy-MM-ddd") != DateTime.Now.ToString("yyy-MM-ddd"))
                {
                    OptionsModel.LastUpdate = DateTime.Now;
                    Storege.SaveModel(OptionsModel);
                    openbuildtoolswindow(null);
                }
            }

            else if (OptionsModel.UpdateEveryMonth)
            {
                if (OptionsModel.LastUpdate.ToString("yyy-MM") != DateTime.Now.ToString("yyy-MM"))
                {
                    OptionsModel.LastUpdate = DateTime.Now;
                    Storege.SaveModel(OptionsModel);
                    openbuildtoolswindow(null);
                }
            }

            else if (OptionsModel.UpdateEveryWeek)
            {
                CultureInfo info = CultureInfo.CurrentCulture;
                if (info.Calendar.GetWeekOfYear(OptionsModel.LastUpdate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday) != info.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday))
                {
                    OptionsModel.LastUpdate = DateTime.Now;
                    Storege.SaveModel(OptionsModel);
                    openbuildtoolswindow(null);
                }
            }
        }

        private void loadbuildtools()
        {
            BuildTools buildtools = BuildTools.SetBuildTools(OptionsModel.BuildToolsPath);

            if (ServerVersions == null)
                ServerVersions = new ObservableCollection<ServerVersionModel>();

            else
                ServerVersions.Clear();

            foreach (var version in buildtools.GetMinecraftServersVersions())
            {
                ServerVersions.Add(new ServerVersionModel() { Version = version });
            }

            ServerVersionModel = ServerVersions.FirstOrDefault();

        }

        private void loadservers()
        {
            if (Servers is null)
                Servers = new ObservableCollection<ServerModel>();

            else
                Servers.Clear();

            MinecraftServerProperties prop;

            foreach (var server in mcdir.GetMinecraftServers())
            {
                if (server is MinecraftServer)
                {
                    try
                    {
                        prop = server.GetProperties();

                        if (server.HasProperties())
                        {
                            Server = new ServerModel() { Name = server.GetServerFolderName(), MinecraftServer = server, PVP = prop.Pvp, MaxPlayers = int.Parse(prop.MaxPlayers.ToString()), SpawnAnimals = prop.SpawnAnimals, AllowFligth = prop.AllowFlight, MOTD = prop.Motd, Host = prop.ServerIp, Port = prop.ServerPort, OnlineMode = prop.OnlineMode, EnableCommandBlock = prop.EnableCommandBlock, SpawnNPCs = prop.SpawnNpcs };
                            Server.EnableProperties = true;
                            Server.Whitelist = new WhitelistModel();
                            changewhitelistbutton(prop);
                        }
                        else
                        {
                            Server = new ServerModel() { Name = server.GetServerFolderName(), MinecraftServer = server };
                            Server.EnableProperties = false;
                        }

                        Server.FilePath = server.GetServerFileName();

                        Server.Ops = new ObservableCollection<PlayerModel>();

                        foreach (var ops in server.GetOps())
                            Server.Ops.Add(new PlayerModel() { Name = ops.Name, UUID = ops.UUID });

                        Server.EnableModel = new EnableModel();

                        Server.Icon = LoadBitmapImage(server.ServerIconPath);

                        server.MinMemery = 512;
                        server.MaxMemery = 1024;

                        Server.Status = ServerModel.EnableStatus.NotRunning;
                        Server.StartAndStop = new StartAndStopModel();
                        Server.StartAndStop.Text = "Start";
                        Server.StartAndStop.Brushes = GetBrushByHexCode("#384a00");

                        Server.Players = new ObservableCollection<PlayerModel>();

                        foreach (var player in ((MinecraftServer)Server.MinecraftServer).GetWhiteListPlayers())
                            Server.Players.Add(new PlayerModel() { Name = player.Name });

                        Server.Players.CollectionChanged += PropertyChanged_Event;
                        Server.Ops.CollectionChanged += PropertyChanged_Event;

                        Server.Plugins = new ObservableCollection<PluginModel>();

                        foreach (var plugin in ((MinecraftServer)Server.MinecraftServer).LoadPlugins())
                            Server.Plugins.Add(new PluginModel() { Name = plugin.GetFileName(), MinecraftPlugin = plugin });

                        Server.PropertyChanged += PropertyChanged_Event;

                        Server.Plugins.CollectionChanged += (sender, e) =>
                        {
                            Server.EnableModel.EnableRemovePlugin = (Server.Plugins.Count > 0);
                        };

                        enablepropertiesforeunningserver(Server);
                        Servers.Add(Server);
                    }
                    catch (Exception ex)
                    {
                        LogFile.AddLog(ex);
                        MessageBox.Show(ex.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }

            displaynoservers();
        }

        private void displaynoservers()
        {
            if (Servers.Count == 0)
            {
                Server = new ServerModel() { StartAndStop = new StartAndStopModel() { Brushes = Brushes.Gray, Text = "No Servers" } };
                Server.Players = new ObservableCollection<PlayerModel>();
                Server.Plugins = new ObservableCollection<PluginModel>();                
                Server.Whitelist = new WhitelistModel() { Brushes = GetBrushByHexCode("#384a00"), Text = "Enablet" };
                Server.Name = "<No servername enable!>";
                Server.Status = ServerModel.EnableStatus.NoServers;
                enablepropertiesforeunningserver(Server);
            }
        }

        private void changewhitelistbutton(MinecraftServerProperties prop)
        {
            if (prop.WhiteList)
            {
                Server.Whitelist.Brushes = GetBrushByHexCode("#5d1723");
                Server.Whitelist.Text = "Disablet";
            }

            else
            {
                Server.Whitelist.Brushes = GetBrushByHexCode("#384a00");
                Server.Whitelist.Text = "Enablet";
            }
        }

        private void autoselect()
        {
            if (Server.Plugin is null)
                if (Server.Plugins.Count > 0)
                    Server.Plugin = Server.Plugins.FirstOrDefault();

            if (Server.Player is null)
                if (Server.Players.Count > 0)
                    Server.Player = Server.Players.FirstOrDefault();

            if (Server.Op is null)
                if (Server.Players.Count > 0)
                    server.Op = Server.Ops.FirstOrDefault();
        }

        private void PropertyChanged_Event(object sender, PropertyChangedEventArgs e)
        {
            if (sender is ServerModel)
            {
                if (!Server.HasErrors)
                {
                    if (Server.MinecraftServer != null)
                    {
                        if (Server.MinecraftServer is MinecraftServer)
                        {
                            if (e.PropertyName == "Name")
                            {
                                try
                                {
                                    ((MinecraftServer)Server.MinecraftServer).SetServerFolderName(mcdir, Server.Name);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show($"{ ex.ToString() }", "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                                }
                            }

                            else if (e.PropertyName != "Terminal" && e.PropertyName != "Errors" && e.PropertyName != "EnableProperties")
                            {
                                if (((MinecraftServer)Server.MinecraftServer).HasProperties())
                                {
                                    var properties = ((MinecraftServer)Server.MinecraftServer).GetProperties();

                                    string serverip = string.Empty, serverport = string.Empty;

                                    string[] host = new string[] { };

                                    try
                                    {
                                        properties.AllowFlight = Server.AllowFligth;
                                        properties.EnableCommandBlock = Server.EnableCommandBlock;
                                        properties.Motd = Server.MOTD;
                                        properties.Pvp = Server.PVP;
                                        properties.SpawnAnimals = Server.SpawnAnimals;
                                        properties.MaxPlayers = uint.Parse(Server.MaxPlayers.ToString());
                                        properties.OnlineMode = Server.OnlineMode;
                                        properties.ServerIp = Server.Host;
                                        properties.MaxWorldSize = 29999984;
                                        properties.MaxBuildHeight = 255;
                                        properties.ServerPort = Server.Port;
                                        properties.SpawnNpcs = Server.SpawnNPCs;
                                        ((MinecraftServer)Server.MinecraftServer).SetProperties(properties);
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show($"{ ex.ToString() }", "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            else if (sender is ServerLuncherViewModel)
            {
                if (e.PropertyName == "Server")
                {
                    autoselect();
                }
            }

        }

        private void PropertyChanged_Event(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (sender is ObservableCollection<PlayerModel>)
            {
                Server.EnableModel.EnableRemovePlayerFromWhitelist = (Server.Players.Count > 0);
                Server.EnableModel.EnableDeOpPlayer = (Server.Ops.Count > 0);
            }
        }

        /// <summary>
        /// Open for connect window.
        /// </summary>
        private void OpenConnectWindow(object parms)
        {
            WindowHandler<ConnectWindow> windowHandler = new WindowHandler<ConnectWindow>();
            windowHandler.OpenWindowAsDialog();
        }

        private void openaddplayerwindow(object parms)
        {
            if (parms is string)
            {
                CommandParameter = parms;
                Player = new PlayerModel();
                if (((string)parms).ToLower() == "whitelist")
                {
                    WindowHandler<AddPlayerWhitelistWindow> wh = new WindowHandler<AddPlayerWhitelistWindow>();
                    wh.OpenWindowAsDialog(this);
                }

                else if (((string)parms).ToLower() == "operators")
                {
                    WindowHandler<AddPlayerToOperatorWindow> window = new WindowHandler<AddPlayerToOperatorWindow>();
                    window.OpenWindowAsDialog(this);
                }
            }
        }

        private void removeplayerfromwhitelist(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
            {
                try
                {
                    var mcserver = (MinecraftServer)Server.MinecraftServer;
                    var players = mcserver.GetWhiteListPlayers();

                    var mcplayer = players.Find(i => i.Name == Server.Player.Name);
                    mcserver.RemovePlayerFromWhiteList(mcplayer);
                    Server.Players.Remove(Server.Player);
                }
                catch (Exception e)
                {
                    LogFile.AddLog(e);
                    MessageBox.Show($"{ e.ToString() }", "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void showlog(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
                ((MinecraftServer)Server.MinecraftServer).ShowLogInNotePad();
        }

        private void openbuildtoolswindow(object parms)
        {
            try
            {
                WindowHandler<BuildToolsLoadingWindow> whandler = new WindowHandler<BuildToolsLoadingWindow>();
                whandler.GoToWindowOnWindowClose<MinecraftServerLuncherWindow>();
                whandler.SwitchOpenWindow<MinecraftServerLuncherWindow>(0);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void addplayertowhitelist(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
            {
                ((MinecraftServer)Server.MinecraftServer).AddPlayerToWhiteList(new MinecraftPlayer() { Name = Player.Name, UUID = Player.UUID });
                Server.Players.Add(Player);
                Server.Player = Player;
                WindowHandler<AddPlayerWhitelistWindow> wh = WindowHandler<AddPlayerWhitelistWindow>.GetHandler(0);
                wh.CloseWindow();
            }
        }

        private void changewhitelist(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
            {
                var pro = ((MinecraftServer)Server.MinecraftServer).GetProperties();
                ((MinecraftServer)Server.MinecraftServer).EnableWhiteList(pro.WhiteList = !pro.WhiteList);
                changewhitelistbutton(pro);
            }
        }

        private void addplugin(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
            {
                try
                {
                    OpenFileDialog filedialog = new OpenFileDialog();
                    filedialog.Filter = "Jar files(*.jar)|*.jar";

                    if (filedialog.ShowDialog() == true)
                    {
                        var plugin = ((MinecraftServer)Server.MinecraftServer).AddPlugin(filedialog.FileName);

                        try
                        {
                            Server.Plugins.Add(new PluginModel() { Name = plugin.GetFileName(), MinecraftPlugin = plugin });
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.ToString(), "Error Exception");
                        }
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show($"{ e.ToString() }", "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void removeplugin(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
            {
                try
                {
                    ((MinecraftServer)Server.MinecraftServer).RemovePlugin(Server.Plugin.MinecraftPlugin);
                    Server.Plugins.Remove(Server.Plugin);
                }
                catch (Exception e)
                {
                    LogFile.AddLog(e);
                    MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void closeapp(object parms)
        {
            WindowHandler<MinecraftServerLuncherWindow> wh = WindowHandler<MinecraftServerLuncherWindow>.GetHandler(0);
            wh.CloseWindow();
        }

        private void openoptionswindow(object parms)
        {
            WindowHandler<OptionsWindow> wh = new WindowHandler<OptionsWindow>();
            wh.OpenWindowAsDialog(new OptionsViewModel());
        }

        private void removeserver(object parms)
        {
            try
            {
                mcdir.DeleteServer(Server.Name);
                Server.PropertyChanged -= PropertyChanged_Event;

                Servers.Remove(Server);

                if (Servers.Count > 0)
                    Server = Servers.Last();

                displaynoservers();

            }
            catch (Exception e)
            {
                LogFile.AddLog(e);
                MessageBox.Show($"{ e.ToString() }", "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void closeaddplayertowhitelistwindow(object parms)
        {
            WindowHandler<AddPlayerWhitelistWindow> wh = WindowHandler<AddPlayerWhitelistWindow>.GetHandler(0);
            wh.CloseWindow();
        }

        private void closeaddopwindow(object parms)
        {
            WindowHandler<AddPlayerToOperatorWindow> window = new WindowHandler<AddPlayerToOperatorWindow>();
            window.CloseWindow();
        }

        private void enablepropertiesforeunningserver(ServerModel server)
        {
            if (server.EnableModel is null)
                server.EnableModel = new EnableModel();

            var status = server.Status;

            switch (status)
            {
                case ServerModel.EnableStatus.Starting:
                    server.EnableModel.EnableOpPlayer = false;
                    server.EnableModel.EnableAddPlayerToWhitelist = false;
                    server.EnableModel.EnableRemovePlayerFromWhitelist = false;
                    server.EnableModel.EnableEnableWhitelist = false;
                    server.EnableModel.EnableHost = false;
                    server.EnableModel.EnableIcon = false;
                    server.EnableModel.EnableLog = false;
                    server.EnableModel.EnableSpawnAnimails = false;
                    server.EnableModel.EnableSpawnNpcs = false;
                    server.EnableModel.EnableEnableCommandBlock = false;
                    server.EnableModel.EnableAllowFligth = false;
                    server.EnableModel.EnableMaxPlayers = false;
                    server.EnableModel.EnableName = false;
                    server.EnableModel.EnableOnlineMode = false;
                    server.EnableModel.EnableRemovePlugin = false;
                    server.EnableModel.EnableAddPlugin = false;
                    server.EnableModel.EnableMotd = false;
                    server.EnableModel.EnablePvp = false;
                    server.EnableModel.EnableRemoveServer = false;
                    server.EnableModel.EnableDeOpPlayer = false;
                    break;
                case ServerModel.EnableStatus.Running:
                    server.EnableModel.EnableAddPlayerToWhitelist = true;
                    server.EnableModel.EnableRemovePlayerFromWhitelist = (Server.Players.Count > 0);
                    server.EnableModel.EnableOpPlayer = true;
                    server.EnableModel.EnableEnableWhitelist = true;
                    server.EnableModel.EnableRemoveServer = false;
                    server.EnableModel.EnableHost = false;
                    server.EnableModel.EnableIcon = false;
                    server.EnableModel.EnableLog = false;
                    server.EnableModel.EnableSpawnAnimails = false;
                    server.EnableModel.EnableSpawnNpcs = false;
                    server.EnableModel.EnableEnableCommandBlock = false;
                    server.EnableModel.EnableAllowFligth = false;
                    server.EnableModel.EnableMaxPlayers = false;
                    server.EnableModel.EnableName = false;
                    server.EnableModel.EnableOnlineMode = false;
                    server.EnableModel.EnableRemovePlugin = false;
                    server.EnableModel.EnableAddPlugin = false;
                    server.EnableModel.EnableMotd = false;
                    server.EnableModel.EnableDeOpPlayer = (Server.Ops.Count > 0);
                    server.EnableModel.EnablePvp = false;
                    break;
                case ServerModel.EnableStatus.NotRunning:
                    server.EnableModel.EnableRemoveServer = true;
                    server.EnableModel.EnableOpPlayer = false;
                    server.EnableModel.EnableAddPlayerToWhitelist = false;
                    server.EnableModel.EnableRemovePlayerFromWhitelist = false;
                    server.EnableModel.EnableEnableWhitelist = false;
                    server.EnableModel.EnableHost = true;
                    server.EnableModel.EnableIcon = true;
                    server.EnableModel.EnableLog = true;
                    server.EnableModel.EnableSpawnAnimails = true;
                    server.EnableModel.EnableSpawnNpcs = true;
                    server.EnableModel.EnableEnableCommandBlock = true;
                    server.EnableModel.EnableAllowFligth = true;
                    server.EnableModel.EnableMaxPlayers = true;
                    server.EnableModel.EnableName = true;
                    server.EnableModel.EnableOnlineMode = true;
                    server.EnableModel.EnableRemovePlugin = true;
                    server.EnableModel.EnableAddPlugin = true;
                    server.EnableModel.EnableMotd = true;
                    server.EnableModel.EnablePvp = true;
                    server.EnableModel.EnableDeOpPlayer = false;
                    break;
                case ServerModel.EnableStatus.NoServers:
                    server.EnableModel.EnableDeOpPlayer = false;
                    server.EnableModel.EnableRemoveServer = false;
                    server.EnableModel.EnableOpPlayer = false;
                    server.EnableModel.EnableAddPlayerToWhitelist = false;
                    server.EnableModel.EnableRemovePlayerFromWhitelist = false;
                    server.EnableModel.EnableEnableWhitelist = false;
                    server.EnableModel.EnableHost = false;
                    server.EnableModel.EnableIcon = false;
                    server.EnableModel.EnableLog = false;
                    server.EnableModel.EnableSpawnAnimails = false;
                    server.EnableModel.EnableSpawnNpcs = false;
                    server.EnableModel.EnableEnableCommandBlock = false;
                    server.EnableModel.EnableAllowFligth = false;
                    server.EnableModel.EnableMaxPlayers = false;
                    server.EnableModel.EnableName = false;
                    server.EnableModel.EnableOnlineMode = false;
                    server.EnableModel.EnableRemovePlugin = false;
                    server.EnableModel.EnableAddPlugin = false;
                    server.EnableModel.EnableMotd = false;
                    server.EnableModel.EnablePvp = false;
                    break;
            }

            if (status != ServerModel.EnableStatus.NoServers)
            {
                server.EnableModel.EnableRemovePlugin = (server.Plugins.Count > 0);
            }
        }

        private void stopserver(object sender, EventArgs e)
        {
            try
            {
                var process = (Process)sender;

                var serv = Servers.FirstOrDefault(i => i.MinecraftServer is MinecraftServer && ((MinecraftServer)i.MinecraftServer).JavaServices.Id == process.Id);

                if (serv.Status == ServerModel.EnableStatus.Running || serv.Status == ServerModel.EnableStatus.Starting)
                {
                    serv.Status = ServerModel.EnableStatus.NotRunning;
                    enablepropertiesforeunningserver(serv);

                    ((MinecraftServer)serv.MinecraftServer).DisableTerminalEvent();

                    if (OptionsModel.ReadTextToSpeek)
                        TextToSpeek("Server is closed down");

                    serv.StartAndStop.Brushes = GetBrushByHexCode("#384a00");
                    serv.StartAndStop.Text = "Start";
                }
            }
            catch (Exception ex)
            {
                LogFile.AddLog(ex);
                MessageBox.Show(ex.Message, "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void startandstopserver(object parms)
        {
            if (Servers.Count == 0)
                return;

            if (!(Server.MinecraftServer is MinecraftServer))
                return;

            if (Server.Status == ServerModel.EnableStatus.Starting)
                return;

            if (!((MinecraftServer)Server.MinecraftServer).JavaServices.IsRunning())
            {
                try
                {
                    Server.Terminal = string.Empty;

                    ((MinecraftServer)Server.MinecraftServer).StartExceptionEvent += (e) =>
                    {
                        LogFile.AddLog(e);
                        MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                        stopserver(null, null);
                    };

                    ((MinecraftServer)Server.MinecraftServer).StartAsync().ConfigureAwait(true);

                    while (((MinecraftServer)Server.MinecraftServer).JavaServices.Process == null && ((MinecraftServer)Server.MinecraftServer).Exception == null) ;

                    if (((MinecraftServer)Server.MinecraftServer).JavaServices.IsRunning())
                    {
                        ((MinecraftServer)Server.MinecraftServer).EnableTerminalEvent();
                        ((MinecraftServer)Server.MinecraftServer).TerminalEvent += (sender, e) =>
                        {
                            try
                            {
                                var server = Servers.First(i => i.MinecraftServer == sender);

                                if (((MinecraftServer)Server.MinecraftServer).Exception == null)
                                    if (e != null)
                                    {
                                        if (((MinecraftServer)Server.MinecraftServer).IsDone && server.Status == ServerModel.EnableStatus.Starting)
                                        {
                                            server.StartAndStop.Brushes = GetBrushByHexCode("#5d1723");
                                            server.StartAndStop.Text = "Stop";
                                            server.Status = ServerModel.EnableStatus.Running;
                                            enablepropertiesforeunningserver(server);
                                            if (OptionsModel.ReadTextToSpeek)
                                                TextToSpeek("Server is started and ready to play on");
                                        }

                                        string term = $"{e}{Environment.NewLine}";

                                        if (term.Contains("WARN]:"))
                                            if (OptionsModel.ReadTextToSpeek)
                                                TextToSpeek($"Warning: {term.Split(':').Last()}");

                                        server.Terminal += term;
                                    }
                            }
                            catch (Exception ex)
                            {
                                LogFile.AddLog(ex);
                                MessageBox.Show(ex.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        };

                        ((MinecraftServer)Server.MinecraftServer).JavaServices.Process.Disposed += stopserver;
                        ((MinecraftServer)Server.MinecraftServer).JavaServices.Process.Exited += stopserver;
                        Server.StartAndStop.Brushes = Brushes.Orange;
                        Server.StartAndStop.Text = "Starting up...";
                        Server.Status = ServerModel.EnableStatus.Starting;
                        enablepropertiesforeunningserver(server);
                        if (OptionsModel.ReadTextToSpeek)
                            TextToSpeek("Server starting up");
                    }
                }
                catch (Exception e)
                {
                    LogFile.AddLog(e);
                    MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            else
            {
                try
                {
                    ((MinecraftServer)Server.MinecraftServer).StopAsync().ConfigureAwait(true);
                    Server.StartAndStop.Brushes = Brushes.Orange;
                    Server.StartAndStop.Text = "Stopping...";
                }
                catch (Exception e)
                {
                    LogFile.AddLog(e);
                    MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }


        }

        private void addserver(object parms)
        {
            try
            {
                MinecraftServerProperties prop = new MinecraftServerProperties();
                prop.ServerPort = "25565";
                prop.ServerIp = "localhost";
                prop.Motd = "A Minecraft Server";
                prop.OpPermissionLevel = 4;
                prop.Pvp = true;
                prop.Difficulty = 1;
                prop.LevelType = "DEFAULT";
                prop.SpawnMonsters = true;
                prop.SpawnAnimals = true;
                prop.MaxWorldSize = 29999984;
                prop.AllowNether = true;
                prop.OnlineMode = true;
                prop.SpawnNpcs = true;
                prop.SnooperEnabled = true;
                prop.NetworkCompressionThreshold = 256;
                prop.LevelName = "world";
                prop.ViewDistance = 10;
                prop.SpawnNpcs = true;
                prop.MaxPlayers = 10;
                prop.MaxBuildHeight = 255;
                prop.AllowFlight = true;
                var server = mcdir.CreateServer($"{ OptionsModel.BuildToolsPath }\\{ ServerVersionModel.Version }", true);
                server.MinMemery = 512;
                server.MaxMemery = 1024;
                server.SetProperties(prop);

                MinecraftServerEula eula = new MinecraftServerEula(server);

                if (!eula.IsAccept())
                {
                    eula.AcceptEula = true;
                    eula.Save();
                }

                var servermodel = new ServerModel();
                servermodel.AllowFligth = prop.AllowFlight;
                servermodel.EnableCommandBlock = prop.EnableCommandBlock;
                servermodel.Host = prop.ServerIp;
                servermodel.Port = prop.ServerPort;
                servermodel.MOTD = prop.Motd;
                servermodel.PVP = prop.Pvp;
                servermodel.SpawnNPCs = prop.SpawnNpcs;
                servermodel.SpawnAnimals = prop.SpawnAnimals;
                servermodel.EnableProperties = true;
                servermodel.MaxPlayers = (int)prop.MaxPlayers;
                servermodel.Name = server.GetServerFolderName();
                servermodel.StartAndStop = new StartAndStopModel() { Text = "Start", Brushes = GetBrushByHexCode("#384a00") };
                servermodel.MinecraftServer = server;
                servermodel.OnlineMode = prop.OnlineMode;
                servermodel.PropertyChanged += PropertyChanged_Event;
                servermodel.MinecraftServer = server;
                servermodel.FilePath = server.GetServerFileName();
                servermodel.Plugins = new ObservableCollection<PluginModel>();
                servermodel.Players = new ObservableCollection<PlayerModel>();
                servermodel.Ops = new ObservableCollection<PlayerModel>();
                servermodel.Ops.CollectionChanged += PropertyChanged_Event;
                servermodel.Players.CollectionChanged += PropertyChanged_Event;
                servermodel.Icon = LoadBitmapImage(server.ServerIconPath);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    servermodel.Whitelist = new WhitelistModel() { Brushes = GetBrushByHexCode("#384a00"), Text = "Enablet" };
                });

                servermodel.Plugins.CollectionChanged += (sender, e) =>
                {
                    Server.EnableModel.EnableRemovePlugin = (Server.Plugins.Count > 0);
                };

                servermodel.Status = ServerModel.EnableStatus.NotRunning;
                enablepropertiesforeunningserver(servermodel);
                Servers.Add(servermodel);
                Server = servermodel;
                if (OptionsModel.ReadTextToSpeek)
                    TextToSpeek("Server now added to serverlist.");

            }
            catch (Exception e)
            {
                LogFile.AddLog(e);
                MessageBox.Show($"{ e.ToString() }", "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool isportinuse(string port)
        {
            return Servers.ToList().Exists(i => i.Port == port);
        }

        private void openaboutewindow(object parms)
        {
            WindowHandler<AbouteWindow> wh = new WindowHandler<AbouteWindow>();
            wh.OpenWindowAsDialog();
        }

        private void openiconwindow(object parms)
        {
            WindowHandler<IconWindow> wh = new WindowHandler<IconWindow>();
            wh.OpenWindowAsDialog(new IconViewModel() { Server = Server });
        }

        private void deopplayer(object parms)
        {
            try
            {
                Server.MinecraftServer.DeOpPlayer(new MinecraftPlayer() { Name = Server.Op.Name });
                Server.Ops.Remove(Server.Op);
            }
            catch (Exception e)
            {
                LogFile.AddLog(e);
                MessageBox.Show(e.Message, "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void addplayertooperator(object parms)
        {
            try
            {
                Server.MinecraftServer.OpPlayer(new MinecraftPlayer() { Name = Player.Name, UUID = Player.UUID });
                Server.Ops.Add(Player);
                Server.Op = Player;
                WindowHandler<AddPlayerToOperatorWindow> window = new WindowHandler<AddPlayerToOperatorWindow>();
                window.CloseWindow();
            }
            catch (Exception e)
            {
                LogFile.AddLog(e);
                MessageBox.Show(e.Message, "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
