﻿using Microsoft.Win32;
using MinecraftServerLuncher.Commands;
using MinecraftServerLuncher.Models;
using MSF.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.ViewModels
{
    public class IconViewModel : ViewModel
    {
        private ServerModel server;

        public Command ChangeIconCommand { get; set; }
        public Command RemoveIconCommand { get; set; }

        public WindowCommand<IconWindow> CloseWindowCommand { get; set; }

        public IconViewModel()
        {
            ChangeIconCommand = new Command(changeicon);
            RemoveIconCommand = new Command(removeicon);
            CloseWindowCommand = new WindowCommand<IconWindow>(WindowCommand<IconWindow>.WindowAction.Close);
        }

        public ServerModel Server
        {
            get => server;

            set
            {
                server = value;
                OnPropertyChanged("Server");
            }
        }

        private void removeicon(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
            {
                ((MinecraftServer)Server.MinecraftServer).RemoveIcon();
                Server.Icon = null;
            }
        }

        private void changeicon(object parms)
        {
            if (Server.MinecraftServer is MinecraftServer)
            {
                OpenFileDialog filedialog = new OpenFileDialog();
                filedialog.Filter = "PNG files(*.png)|*.png";

                if (filedialog.ShowDialog() == true)
                {
                    if (filedialog.FileName != "")
                    {
                        ((MinecraftServer)Server.MinecraftServer).SetServerIcon(filedialog.FileName);
                        Server.Icon = LoadBitmapImage(filedialog.FileName);
                    }
                }
            }
        }
    }
}
