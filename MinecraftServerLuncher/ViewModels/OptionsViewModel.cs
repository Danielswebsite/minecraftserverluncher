﻿using MinecraftServerLuncher.Commands;
using MSF.Tools;
using MinecraftServerLuncher.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MinecraftServerLuncher.Storege;
using System.IO;
using MinecraftServerLuncher.Handlers;

// https://www.wpf-tutorial.com/da/54/almindelige-interface-kontroller/ribbon-kontrollen/
// https://docs.microsoft.com/en-us/dotnet/api/system.windows.controls.ribbon.ribbon?view=netframework-4.8

namespace MinecraftServerLuncher.ViewModels
{
    public class OptionsViewModel : ViewModel
    {
        public OptionsModel OptionsModel { get; set; }

        public ObservableCollection<ServerVersionModel> ServerVersionModels { get; set; }

        public ServerVersionModel SelectedVersionModel { get; set; }

        public WindowCommand<OptionsWindow> CloseWindowCommand { get; set; }

        private bool serverlocationhaschange;
        private bool buildtoolslocationhaschange;

        public OptionsViewModel()
        {
            try
            {
                OptionsModel = Storege.LoadModel();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            loadminecraftversions();
            
            SaveCommand = new Command(saveoptions);
            CloseWindowCommand = new WindowCommand<OptionsWindow>(WindowCommand<OptionsWindow>.WindowAction.Close);

            OptionsModel.PropertyChanged += (sender, e) =>
            {
                if (e.PropertyName.Equals("MinecraftServersPath"))
                {
                    serverlocationhaschange = true;
                }

                if (e.PropertyName.Equals("BuildToolsPath"))
                {
                    buildtoolslocationhaschange = true;
                }
            };
        }

        private void loadminecraftversions()
        {
            var buildtools = BuildTools.GetBuildTools();

            ServerVersionModels = new ObservableCollection<ServerVersionModel>();

            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.18.1", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.18", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.15.1", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.15.0", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.14.4", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.14.3", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.14.2", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.14.1", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.14.0", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.13.2", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.13.1", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.13.0", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.12.2", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.12.1", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.12.0", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.11.2", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.11.1", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.11.0", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.10.2", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.10.1", BuildTools = buildtools });
            ServerVersionModels.Add(new ServerVersionModel() { Version = "1.10.0", BuildTools = buildtools });

            SelectedVersionModel = ServerVersionModels.FirstOrDefault(i => i.Version == OptionsModel.TargetMiencraftVersion.Version);

        }

        public Command SaveCommand { get; set; }

        private void saveoptions(object parms)
        {
            try
            {
                if (!OptionsModel.HasErrors)
                {
                    if (serverlocationhaschange)
                        if (!Directory.Exists(OptionsModel.MinecraftServersPath))
                            Directory.CreateDirectory(OptionsModel.MinecraftServersPath);

                    if (buildtoolslocationhaschange)
                        if (!Directory.Exists(OptionsModel.BuildToolsPath))
                            Directory.CreateDirectory(OptionsModel.BuildToolsPath);
                    
                    OptionsModel.TargetMiencraftVersion = SelectedVersionModel;
                    Storege.SaveModel(OptionsModel);
                    
                    WindowHandler<OptionsWindow> wh = WindowHandler<OptionsWindow>.GetHandler(0);
                    wh.CloseWindow();

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
