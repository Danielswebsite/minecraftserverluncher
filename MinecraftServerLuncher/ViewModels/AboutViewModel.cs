﻿using MinecraftServerLuncher.Commands;
using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.ViewModels
{
    public class AboutViewModel : ViewModel
    {

        public WindowCommand<AbouteWindow> CloseWindowCommand { get; set; }

        private string version;

        public string Version
        {
            get
            {
                return version;
            }

            set
            {
                version = value;
                OnPropertyChanged("Version");
            }
        }


        public AboutViewModel()
        {
            CloseWindowCommand = new WindowCommand<AbouteWindow>(WindowCommand<AbouteWindow>.WindowAction.Close);

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment application = ApplicationDeployment.CurrentDeployment;
                Version = application.CurrentVersion.ToString();
            }
            else
            {
                Version = "Delvelemt Version!";
            }
        }
    }
}
