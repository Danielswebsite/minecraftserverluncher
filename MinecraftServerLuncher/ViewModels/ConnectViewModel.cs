﻿using MinecraftServerLuncher.Commands;
using MinecraftServerLuncher.Handlers;
using MinecraftServerLuncher.Models;
using MinecraftServerLuncher.Storege;
using MSF.Network;
using MSF.Network.Minecarft;
using MSF.Network.Packet;
using MSF.Network.Packet.Body;
using MSF.Network.Security;
using MSF.Network.Security.Maneger;
using MSF.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftServerLuncher.ViewModels
{
    public class ConnectViewModel : ViewModel
    {
        public WindowCommand<ConnectWindow> CloseConnectWindow { get; set; }

        public Command ConnectCommand { get; set; }

        private ConnectModel connectmodel;

        private HostLoginManeger<HostUser> hostLoginManeger;

        public ConnectModel ConnectModel
        {
            get => connectmodel;

            set
            {
                connectmodel = value;
                OnPropertyChanged("ConnectModel");
            }
        }

        public ConnectViewModel()
        {
            ConnectModel = new ConnectModel();
            CloseConnectWindow = new WindowCommand<ConnectWindow>(WindowCommand<ConnectWindow>.WindowAction.Close);
            ConnectCommand = new Command(Connect);

            App.Client.ConnectedEvent += Connect_Event;
        }

        private void Login_Event(object sender, HostUserLoginEventArgs e) 
        {
            WindowHandler<ConnectWindow> window = new WindowHandler<ConnectWindow>();

            Application.Current.Dispatcher?.Invoke(() =>
            {
                if (sender is HostUser)
                    App.User = (HostUser)sender;
                window.CloseWindow();
            });
            hostLoginManeger.LoginEvent -= Login_Event;
        }

        private void LoginFail_Event(object sender, HostUserLoginFailEventArgs e)
        {
            MessageBox.Show("Login fail wraong username or password.", "Login fail");
            hostLoginManeger.LoginFailEvent -= LoginFail_Event;
        }

        private void Connect_Event(object sender, HostEventArgs e)
        {
            if (sender is HostConnection)
            {
                HostConnection hostConnection = (HostConnection)sender;

                hostLoginManeger = new HostLoginManeger<HostUser>(App.Client, hostConnection);

                hostLoginManeger.SendLoginRequest(connectmodel.UserName, connectmodel.Password);
                hostLoginManeger.LoginEvent += Login_Event;
                hostLoginManeger.LoginFailEvent += LoginFail_Event;
            }
        }

        /// <summary>
        /// Conect to host server
        /// </summary>
        /// <param name="parms"></param>
        private void Connect(object parms)
        {
            try
            {                
                App.Client.ConnectAsync(ConnectModel.Host, ConnectModel.Port);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

    }
}
