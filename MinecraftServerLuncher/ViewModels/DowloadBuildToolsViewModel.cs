﻿using MSF.Tools;
using MinecraftServerLuncher.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using MinecraftServerLuncher.Log;
using MSF.Java;
using MinecraftServerLuncher.Exceptions;
using MinecraftServerLuncher.Commands;
using MinecraftServerLuncher.Handlers;

namespace MinecraftServerLuncher.ViewModels
{
    public class DowloadBuildToolsViewModel : ViewModel
    {
        public ProgressModel ProgressModel { get; set; }

        private BuildTools buildtools = BuildTools.GetBuildTools();

        private JavaExicuter java;

        private async void downloadasync()
        {
            await Task.Run(async () =>
            {
                try
                {
                    ProgressModel.ProgressText = "Dowloader BuildTools.jar...";

                    if (!JavaExicuter.JavaIsInstaled())
                        throw new MinecraftServerLuncherException("Java is not is not instaled! Please install java 64 bit from: https://www.java.com/en/download/manual.jsp");

                    ProgressModel.ProgressText = $"Clare buildtools files...";
                    buildtools.ClearBuildTools();

                    buildtools.DownloadBuildTools();
                    ProgressModel.ProgressText = $"Running BuildTools setup...";

                    java = buildtools.RunBuildTools(Storege.LoadModel().TargetMiencraftVersion.Version);

                    while (java.Process == null);
                    java.Process.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data != null)
                            ProgressModel.ProgressOutput += $"{e.Data}\n";
                    };

                    while (java.IsRunning());

                    ProgressModel.ProgressText = "Done!";

                    OptionsModel options = Storege.LoadModel();
                    options.LastUpdate = DateTime.Now;
                    Storege.SaveModel(options);

                    await System.Threading.Tasks.Task.Delay(300);
                }
                catch (Exception e)
                {
                    if (!(e is MinecraftServerLuncherException))
                        LogFile.AddLog(e);

                    MessageBox.Show(e.Message, "Error Exception");
                }

                finally
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        WindowHandler<BuildToolsLoadingWindow> whandler = new WindowHandler<BuildToolsLoadingWindow>();
                        whandler.CloseWindow();
                    });
                }
            });
        }

        public Command CancelDownloadCommand { get; set; }

        public WindowCommand<BuildToolsLoadingWindow> CloseWindowCommand { get; set; }

        public DowloadBuildToolsViewModel()
        {
            ProgressModel = new ProgressModel();
            CloseWindowCommand = new WindowCommand<BuildToolsLoadingWindow>(WindowCommand<BuildToolsLoadingWindow>.WindowAction.Close);
            CancelDownloadCommand = new Command(canceldownload);
            downloadasync();
        }

        private void canceldownload(object parms)
        {
            if (java != null)
                java.Stop();

            WindowHandler<BuildToolsLoadingWindow> whandler = new WindowHandler<BuildToolsLoadingWindow>();
            whandler.CloseWindow();
        }
    }
}
