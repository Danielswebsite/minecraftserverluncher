﻿using MinecraftServerLuncher.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MinecraftServerLuncher.Exceptions
{
    /// <summary>
    /// Exception created special for this software. 
    /// </summary>
    public sealed class MinecraftServerLuncherException : Exception
    {
        /// <summary>
        /// Make exeception based on this sowftware to for a exception handling.
        /// This exception will create a lofile and log exceptions in to it.
        /// </summary>
        public MinecraftServerLuncherException() : base()
        {
            LogFile.AddLog(new LogMessage() { LogCode = LogMessage.Code.Exception, Description = Message });
        }

        /// <summary>
        /// Make exeception based on this sowftware to for a exception handling with a spical message.
        /// This exception will create a lofile and log exceptions in to it.
        /// </summary>
        /// <param name="message"></param>
        public MinecraftServerLuncherException(string message) : base(message)
        {
            LogFile.AddLog(new LogMessage() { LogCode = LogMessage.Code.Exception, Description = Message });
        }
    }
}
