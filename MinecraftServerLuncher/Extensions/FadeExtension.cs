﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MinecraftServerLuncher.Extensions
{
    /// <summary>
    /// Exstendiosn to usercontrols for fade out afther a time. 
    /// </summary>
    public class FadeExtension : UserControl
    {
        /// <summary>
        /// Dedcate a XAML attrbute property there take a datatype boolean to set the usercontroll to fade out or not.
        /// The usercontrol will fade out if this are set to true.
        /// </summary>
        public static readonly DependencyProperty AutoFadeProperty = DependencyProperty.RegisterAttached("AutoFade", typeof(bool), typeof(FadeExtension), new PropertyMetadata(false, AutoFadeChange_Event));

        /// <summary>
        /// Set the value of the XAML attrbute property
        /// </summary>
        /// <param name="d"></param>
        /// <param name="value"></param>
        public static void SetAutoFade(UserControl d, bool value)
        {
            d.SetValue(AutoFadeProperty, value);
        }

        /// <summary>
        /// Get the value of the XAML attrbute property
        /// </summary>
        public static bool GetAutoFade(UserControl d)
        {
            return (bool)d.GetValue(AutoFadeProperty);
        }

        /// <summary>
        /// Change event there will be caled on the XAML attrbute property and the class are loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void AutoFadeChange_Event(object sender, DependencyPropertyChangedEventArgs e)
        {            
            Task.Run(() =>
            {
                if (sender is UserControl)
                {
                    UserControl control = (UserControl)sender;

                    Thread.Sleep(1000 * 1);

                    for (double i = 1.0; i > 0.0; i -= 0.1)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            control.Opacity = i;
                        });
                        Thread.Sleep(100);
                    }

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        control.Visibility = Visibility.Collapsed;
                    });
                }
            });
        }
    }
}
