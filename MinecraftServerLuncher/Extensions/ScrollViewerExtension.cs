﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

//https://stackoverflow.com/questions/2984803/how-to-automatically-scroll-scrollviewer-only-if-the-user-did-not-change-scrol

namespace MinecraftServerLuncher.Extensions
{
    public class ScrollViewerExtension : ScrollViewer
    {
        public static readonly DependencyProperty AutoScrollProperty = DependencyProperty.RegisterAttached("AutoScroll", typeof(bool), typeof(ScrollViewerExtension), new PropertyMetadata(false, AutoScrollChange_Event));

        public static void SetAutoScroll(ScrollViewer d, bool value)
        {
            d.SetValue(AutoScrollProperty, value);
        }

        public static bool GetAutoScroll(ScrollViewer d)
        {
            return (bool)d.GetValue(AutoScrollProperty);
        }

        private static void AutoScrollChange_Event(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is ScrollViewer)
            {
                ScrollViewer viewer = sender as ScrollViewer;
                bool autoscroll = (e.NewValue != null) && (bool)e.NewValue;

                if (autoscroll)
                {
                    viewer.ScrollChanged += ScrollChange;
                }
                else
                {
                    viewer.ScrollChanged -= ScrollChange;
                }
            }
        }

        private static double scrollheigth;

        private static void ScrollChange(object sender, ScrollChangedEventArgs e)
        {
            if (sender is ScrollViewer)
            {
                ScrollViewer viewer = (ScrollViewer)sender;
                if (GetAutoScroll(viewer))
                {
                    if (scrollheigth != viewer.ScrollableHeight)
                    {
                        scrollheigth = viewer.ScrollableHeight;
                        viewer.ScrollToEnd();
                    }
                }
            }
        }
    }
}
