﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Models
{
    public class PlayerModel : Model
    {
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(ref name, "Name");
            }
        }

        private string uuid;
        public string UUID
        {
            get
            {
                return uuid; 
            }

            set
            {
                uuid = value;
                OnPropertyChanged(ref uuid, "UUID");
            }

        }

        
    }
}
