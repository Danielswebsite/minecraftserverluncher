﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace MinecraftServerLuncher.Models
{
    // https://www.eidias.com/blog/2013/7/13/utilizing-inotifydataerrorinfo-in-wpf-mvvm-app
    // https://stackoverflow.com/questions/28844518/bindablebase-vs-inotifychanged
    // https://www.tutorialspoint.com/mvvm/mvvm_validations.htm

    public abstract class Model : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        public bool HasErrors
        {
            get
            {
                return Errors.Count > 0;
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged = delegate { };

        [XmlIgnore]
        public Dictionary<string, List<string>> Errors { get; set; } = new Dictionary<string, List<string>>();


        public IEnumerable GetErrors(string propertyName)
        {
            if (Errors.ContainsKey(propertyName))
                return Errors[propertyName];
            return null;
        }

        private void ValidateProperty<T>(string propertyName, T value)
        {
            var result = new List<System.ComponentModel.DataAnnotations.ValidationResult>();

            ValidationContext context = new ValidationContext(this);
            context.MemberName = propertyName;
            Validator.TryValidateProperty(value, context, result);

            if (result.Any())
            {
                Errors[propertyName] = result.Select(i => i.ErrorMessage).ToList();
            }
            else
            {
                Errors.Remove(propertyName);
            }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Errors"));
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged<T>(ref T value, string propertyName)
        {
            ValidateProperty(propertyName, value);
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
