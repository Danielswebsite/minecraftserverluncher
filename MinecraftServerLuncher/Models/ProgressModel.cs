﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Models
{
    public class ProgressModel : Model
    {
        private string progresstext;

        public string ProgressText
        {
            get
            {
                return progresstext;
            }

            set
            {
                progresstext = value;
                OnPropertyChanged(ref progresstext, "ProgressText");
            }
        }

        private string progressoutput;
        public string ProgressOutput
        {
            get
            {
                return progressoutput;                
            }
            set
            {
                progressoutput = value;
                OnPropertyChanged(ref progressoutput, "ProgressOutput");
            }
        }

    }
}
