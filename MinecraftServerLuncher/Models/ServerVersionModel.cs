﻿using MSF.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MinecraftServerLuncher.Models
{
    [Serializable]
    public class ServerVersionModel : Model
    {
        private string version;

        [XmlElement]
        public string Version
        {
            get { return version; }
            set
            {
                version = value;
                OnPropertyChanged(ref version, "Version");
            }
        }


        private BuildTools buildtools;

        [XmlIgnore]
        public BuildTools BuildTools
        {
            get { return buildtools; }
            set
            {
                buildtools = value;
                OnPropertyChanged(ref buildtools, "BuildTools");
            }
        }

    }
}
