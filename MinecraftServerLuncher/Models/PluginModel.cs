﻿using MSF.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Models
{
    public class PluginModel : Model
    {        
        public MinecraftPlugin MinecraftPlugin { get; set; }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(ref name,  "Name");
            }
        }
    }
}
