﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MinecraftServerLuncher.Models
{
    public class AlertModel : Model
    {
        private SolidColorBrush background;
        public SolidColorBrush BackGround 
        {
            get => background;

            set 
            {
                background = value;
                OnPropertyChanged(ref background, "BackGround");
            }
        }  
    }
}
