﻿using MSF.Plugin;
using MSF.Server;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MinecraftServerLuncher.Models
{    
    public class ServerModel : Model
    {
        public enum EnableStatus
        {
            Starting,
            Running,
            NotRunning,
            NoServers
        }

        private EnableStatus status;

        public EnableStatus Status
        {
            get => status;

            set
            {
                status = value;
                OnPropertyChanged(ref status, "Status");
            }
        }

        private EnableModel enablemodel;

        public EnableModel EnableModel
        {
            get => enablemodel;
            set
            {
                enablemodel = value;
                OnPropertyChanged(ref enablemodel, "EnableModel");
            }
        }


        [Obsolete]
        private bool isrunning;

        [Obsolete]
        public bool IsRunning
        {
            get => isrunning;

            set
            {
                isrunning = value;
                OnPropertyChanged(ref isrunning, "IsRunning");
            }
        }

        [Obsolete]
        private bool isstarting;

        [Obsolete]
        public bool IsStarting
        {
            get
            {
                return isstarting;
            }

            set
            {
                isstarting = value;
                OnPropertyChanged(ref isstarting, "IsStarting");
            }

        }

        private BitmapImage icon;

        public BitmapImage Icon
        {
            get { return icon; }

            set
            {
                icon = value;
                OnPropertyChanged(ref icon, "Icon");
            }
        }


        private string filepath;

        [Required]
        public string FilePath
        {
            get { return filepath; }
            set
            {
                filepath = value;
                OnPropertyChanged(ref filepath, "FilePath");
            }
        }

        private string name;

        [Required]
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged(ref name, "Name");
            }
        }

        private PlayerModel player;
        public PlayerModel Player
        {
            get
            {
                return player;
            }

            set
            {
                player = value;
                OnPropertyChanged(ref player, "Player");
            }
        }

        private PlayerModel op;
        public PlayerModel Op
        {
            get
            {
                return op;
            }

            set
            {
                op = value;
                OnPropertyChanged(ref op, "Op");
            }
        }

        private string terminal;
        public string Terminal
        {
            get
            {
                return terminal;
            }

            set
            {
                terminal = value;
                OnPropertyChanged(ref terminal, "Terminal");
            }
        }

        private StartAndStopModel startandstop;
        public StartAndStopModel StartAndStop
        {
            get
            {
                return startandstop;
            }

            set
            {
                startandstop = value;
                OnPropertyChanged(ref startandstop, "StartAndStop");
            }

        }

        private WhitelistModel whitelist;
        public WhitelistModel Whitelist
        {
            get
            {
                return whitelist;
            }

            set
            {
                whitelist = value;
                OnPropertyChanged(ref whitelist, "Whitelist");
            }
             
        }

        private PluginModel plugin;
        public PluginModel Plugin
        {
            get
            {
                return plugin;  
            }

            set
            {
                plugin = value;
                OnPropertyChanged(ref plugin, "Plugin");
            }

        }

        private IServer minecraftserver;

        public IServer MinecraftServer
        {
            get { return minecraftserver; }
            set
            {
                minecraftserver = value;
                OnPropertyChanged(ref minecraftserver, "MinecraftServer");
            }
        }

        private bool enableproperties;
        public bool EnableProperties
        {
            get { return enableproperties; }
            set
            {
                enableproperties = value;
                OnPropertyChanged(ref enablecommandblock, "EnableProperties");
            }
        }

        private string host;

        [Required]
        public string Host
        {
            get { return host; }
            set
            {
                host = value;
                OnPropertyChanged(ref host, "Host");
            }
        }

        private string port;

        [Required]
        [RegularExpression("^[0-9]*$", ErrorMessage = "This is not a number.")]
        public string Port
        {
            get
            {
                return port;
            }

            set
            {
                port = value;
                OnPropertyChanged(ref port, "Port");
            }
        }

        private string motd;
        [Required]
        [MaxLength(59)]
        public string MOTD
        {
            get { return motd; }
            set
            {
                motd = value;
                OnPropertyChanged(ref motd, "MOTD");
            }
        }

        private int maxplayers;

        [Required]                
        public int MaxPlayers
        {
            get { return maxplayers; }
            set
            {
                maxplayers = value;
                OnPropertyChanged(ref maxplayers, "MaxPlayers");
            }
        }

        private bool pvp;
        public bool PVP
        {
            get { return pvp; }
            set
            {
                pvp = value;
                OnPropertyChanged(ref pvp, "PVP");
            }
        }

        private bool spawnnpcs;
        public bool SpawnNPCs
        {
            get { return spawnnpcs; }
            set
            {
                spawnnpcs = value;
                OnPropertyChanged(ref spawnnpcs, "SpawnNPCs");
            }
        }

        private bool onlinemode;
        public bool OnlineMode
        {
            get { return onlinemode; }
            set
            {
                onlinemode = value;
                OnPropertyChanged(ref onlinemode, "OnlineMode");
            }
        }

        private bool spawnanimals;
        public bool SpawnAnimals
        {
            get { return spawnanimals; }
            set
            {
                spawnanimals = value;
                OnPropertyChanged(ref spawnanimals, "OnlineMode");
            }
        }

        private bool allowfligth;
        public bool AllowFligth
        {
            get { return allowfligth; }
            set
            {
                allowfligth = value;
                OnPropertyChanged(ref allowfligth, "OnlineMode");
            }
        }

        private bool enablecommandblock;
        public bool EnableCommandBlock
        {
            get { return enablecommandblock; }
            set
            {
                enablecommandblock = value;
                OnPropertyChanged(ref enablecommandblock, "OnlineMode");
            }
        }

        public ObservableCollection<PluginModel> Plugins { get; set; }

        public ObservableCollection<PlayerModel> Players { get; set; }

        public ObservableCollection<PlayerModel> Ops { get; set; }

    }
}
