﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Models
{
    public class ConnectModel : Model
    {
        private string username;
        public string UserName 
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged(ref username, "UserName");
            }
        }

        private string password;

        public string Password 
        {
            get => password;
            
            set
            {
                password = value;
                OnPropertyChanged(ref password, "Password");
            }
        }

        private string host;

        public string Host 
        {
            get => host;

            set 
            {
                host = value;
                OnPropertyChanged(ref host, "Host");
            }
        }

        private int port;

        public int Port
        {
            get => port;

            set
            {
                port = value;
                OnPropertyChanged(ref port, "Port");
            }

        }
    }
}
