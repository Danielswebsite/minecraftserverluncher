﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MinecraftServerLuncher.Models
{
    [Serializable]
    [XmlRoot(ElementName = "UserSettings", Namespace = "http://minecraft-server-launcher.com/options")]
    public class OptionsModel : Model
    {
        private string buildtoolspath;

        [Required]        
        [XmlElement]
        [RegularExpression(@"^(?:[a-zA-Z]\:|\\\\[\w\.]+\\[\w.$]+)\\(?:[\w]+\\)*\w([\w.])+$", ErrorMessage = "Dette er ikke gyldig system path!")]
        public string BuildToolsPath
        {
            get { return buildtoolspath; }
            set
            {
                buildtoolspath = value;
                OnPropertyChanged(ref buildtoolspath, "BuildToolsPath");
            }
        }

        private string minecraftserverspath;

        [Required]
        [XmlElement]
        [RegularExpression(@"^(?:[a-zA-Z]\:|\\\\[\w\.]+\\[\w.$]+)\\(?:[\w]+\\)*\w([\w.])+$", ErrorMessage = "Dette er ikke gyldig system path!")]
        public string MinecraftServersPath
        {
            get
            {
                return minecraftserverspath;
            }
            set
            {
                minecraftserverspath = value;
                OnPropertyChanged(ref minecraftserverspath, "MinecraftServersPath");
            }
        }

        private bool updateveryday;
        [XmlElement]
        public bool UpdateEveryDay
        {
            get => updateveryday;
            set
            {
                updateveryday = value;
                OnPropertyChanged(ref updateveryday, "UpdateEveryDay");
            }
        }

        private bool updateverymonth;
        [XmlElement]
        public bool UpdateEveryMonth
        {
            get => updateverymonth;
            set
            {
                updateverymonth = value;
                OnPropertyChanged(ref updateverymonth, "UpdateEveryMonth");
            }
        }

        private bool updateeveryweek;
        [XmlElement]
        public bool UpdateEveryWeek
        {
            get => updateeveryweek;
            set
            {
                updateeveryweek = value;
                OnPropertyChanged(ref updateeveryweek, "UpdateEveryWeek");
            }
        }

        //private bool closewithexit;
        //[XmlElement]
        //public bool CloseWithExit
        //{
        //    get => closewithexit;
        //    set
        //    {
        //        closewithexit = value;
        //        OnPropertyChanged(ref closewithexit, "CloseWithExit");
        //    }
        //}


        private ServerVersionModel targetminecraftversion;

        [XmlElement]
        public ServerVersionModel TargetMiencraftVersion
        {
            get
            {
                return targetminecraftversion;
            }

            set
            {
                targetminecraftversion = value;
                OnPropertyChanged(ref targetminecraftversion, "TargetMiencraftVersion");
            }

        }


        private DateTime lastupdate;

        [XmlElement]
        public DateTime LastUpdate
        {
            get
            {
                return lastupdate;
            }

            set
            {
                lastupdate = value;
                OnPropertyChanged(ref lastupdate, "LastUpdate");
            }
        }

        private bool autoupdatebuildtools;

        [XmlElement]
        public bool AutoOpdateBuildTools
        {
            get
            {
                return autoupdatebuildtools;
            }

            set
            {
                autoupdatebuildtools = value;
                OnPropertyChanged(ref autoupdatebuildtools, "AutoOpdateBuildTools");
            }
        }

        private bool readtexttospeek;

        [XmlElement]
        public bool ReadTextToSpeek
        {
            get
            {
                return readtexttospeek;
            }
            set
            {
                readtexttospeek = value;
                OnPropertyChanged(ref readtexttospeek, "ReadTextToSpeek");
            }
        }
    }
}
