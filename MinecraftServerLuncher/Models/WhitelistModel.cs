﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MinecraftServerLuncher.Models
{
    public class WhitelistModel : Model
    {
        private SolidColorBrush brushes;
        public SolidColorBrush Brushes
        {
            get
            {                
                return brushes;
            }

            set
            {
                brushes = value;
                OnPropertyChanged(ref brushes, "Brushes");
            }
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                OnPropertyChanged(ref text, "Text");
            }
        }

        private bool enable;
        public bool Enable
        {
            get
            {
                return enable;
            }

            set
            {
                enable = value;
                OnPropertyChanged(ref enable, "Enable");
            }
        }
    }
}
