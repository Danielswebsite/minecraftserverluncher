﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Models
{
    public class PathModel : Model
    {
        private string uri;
        public string Uri
        {
            get { return uri; }
            set
            {
                uri = value;
                OnPropertyChanged(ref uri, "Uri");
            }
        }

        private bool isfilepath;
        public bool IsFilePath
        {
            get { return isfilepath; }
            set
            {
                isfilepath = value;
                OnPropertyChanged(ref isfilepath, "IsFilePath");
            }
        }

    }
}
