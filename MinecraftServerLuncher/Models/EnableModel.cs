﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncher.Models
{
    public class EnableModel : Model
    {
        public bool enableaddplayertowhitelist;

        public bool EnableAddPlayerToWhitelist
        {
            get => enableaddplayertowhitelist;

            set
            {
                enableaddplayertowhitelist = value;
                OnPropertyChanged(ref enableaddplayertowhitelist, "EnableAddPlayerToWhitelist");
            }
        }

        private bool enableremoveplayertowhitelist;

        public bool EnableRemovePlayerFromWhitelist
        {
            get => enableremoveplayertowhitelist;

            set
            {
                enableremoveplayertowhitelist = value;
                OnPropertyChanged(ref enableremoveplayertowhitelist, "EnableRemovePlayerFromWhitelist");
            }
        }

        private bool enableenablewhitelist;

        public bool EnableEnableWhitelist
        {
            get => enableenablewhitelist;

            set
            {
                enableenablewhitelist = value;
                OnPropertyChanged(ref enableenablewhitelist, "EnableEnableWhitelist");
            }
        }

        private bool enableopplayer;

        public bool EnableOpPlayer
        {
            get => enableopplayer;

            set
            {
                enableopplayer = value;
                OnPropertyChanged(ref enableopplayer, "EnableOpPlayer");
            }
        }

        private bool enabledeopplayer;

        public bool EnableDeOpPlayer
        {
            get
            {
                return enabledeopplayer;
            }
            set
            {
                enabledeopplayer = value;
                OnPropertyChanged(ref enabledeopplayer, "EnableDeOpPlayer");
            }
        }

        private bool enablename;

        public bool EnableName
        {
            get => enablename;

            set
            {
                enablename = value;
                OnPropertyChanged(ref enablename, "EnableName");
            }
        }

        private bool enableaddplugin;

        public bool EnableAddPlugin
        {
            get => enableaddplugin;

            set
            {
                enableaddplugin = value;
                OnPropertyChanged(ref enableaddplugin, "EnableAddPlugin");
            }
        }

        private bool enableremoveplugin;

        public bool EnableRemovePlugin
        {
            get => enableremoveplugin;

            set
            {
                enableremoveplugin = value;
                OnPropertyChanged(ref enableremoveplugin, "EnableRemovePlugin");
            }
        }

        private bool enablehost;

        public bool EnableHost
        {
            get => enablehost;

            set
            {
                enablehost = value;
                OnPropertyChanged(ref enablehost, "EnableHost");
            }
        }

        private bool enableenablecommandblock;

        public bool EnableEnableCommandBlock
        {
            get => enableenablecommandblock;

            set
            {
                enableenablecommandblock = value;
                OnPropertyChanged(ref enableenablecommandblock, "EnableEnableCommandBlock");
            }
        }

        private bool enablespawnanimails;

        public bool EnableSpawnAnimails
        {
            get => enablespawnanimails;

            set
            {
                enablespawnanimails = value;
                OnPropertyChanged(ref enablespawnanimails, "EnableSpawnAnimails");
            }
        }


        private bool enableallowfligth;

        public bool EnableAllowFligth
        {
            get => enableallowfligth;

            set
            {
                enableallowfligth = value;
                OnPropertyChanged(ref enableallowfligth, "EnableAllowFligth");
            }
        }


        private bool enablepvp;

        public bool EnablePvp
        {
            get => enablepvp;

            set
            {
                enablepvp = value;
                OnPropertyChanged(ref enablepvp, "EnablePvp");
            }
        }

        private bool enablemaxplayers;

        public bool EnableMaxPlayers
        {
            get => enablemaxplayers;

            set
            {
                enablemaxplayers = value;
                OnPropertyChanged(ref enablemaxplayers, "EnableMaxPlayers");
            }

        }

        private bool enablespawnnpcs;

        public bool EnableSpawnNpcs
        {
            get => enablespawnnpcs;
            set
            {
                enablespawnnpcs = value;
                OnPropertyChanged(ref enablespawnnpcs, "EnableSpawnNpcs");
            }
        }

        private bool enableonlinemode;

        public bool EnableOnlineMode
        {
            get => enableonlinemode;

            set
            {
                enableonlinemode = value;
                OnPropertyChanged(ref enableonlinemode, "EnableOnlineMode");
            }
        }

        private bool enablelog;

        public bool EnableLog
        {
            get => enablelog;

            set
            {
                enablelog = value;
                OnPropertyChanged(ref enablelog, "EnableLog");
            }
        }

        private bool enableicon;

        public bool EnableIcon
        {
            get => enableicon;

            set
            {
                enableicon = value;
                OnPropertyChanged(ref enableicon, "EnableIcon");
            }
        }

        private bool enableremoveserver;

        public bool EnableRemoveServer
        {
            get => enableremoveserver;

            set
            {
                enableremoveserver = value;
                OnPropertyChanged(ref enableremoveserver, "EnableRemoveServer");
            }
        }

        private bool enableaddserver;

        public bool EnableAddServer
        {
            get => enableaddserver;
            set
            {
                enableaddserver = value;
                OnPropertyChanged(ref enableaddserver, "EnableAddServer");
            }
        }

        private bool enablemotd;

        public bool EnableMotd
        {
            get => enablemotd;

            set
            {
                enablemotd = value;
                OnPropertyChanged(ref enablemotd, "EnableMotd");
            }
        }
    }
}
