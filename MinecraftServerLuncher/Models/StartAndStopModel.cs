﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MinecraftServerLuncher.Models
{
    public class StartAndStopModel : Model
    {
        private bool isstartet;
        public bool IsStartet
        {
            get => isstartet;
            set
            {
                isstartet = value;
                OnPropertyChanged(ref isstartet, "IsStartet");
            }
        }

        private SolidColorBrush brushes;
        public SolidColorBrush Brushes
        {
            get
            {
                return brushes;
            }

            set
            {
                brushes = value;
                OnPropertyChanged(ref brushes, "Brushes");
            }
        }

        private string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                OnPropertyChanged(ref text, "Text");
            }
        }
        
    }
}
