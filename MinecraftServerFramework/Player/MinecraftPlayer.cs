﻿using MSF.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Player
{
    public class MinecraftPlayer
    {
        public string UUID { get; set; }
        public string Name { get; set; }
    }
}
