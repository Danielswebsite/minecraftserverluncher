﻿using MSF.Network.Packet.Body;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet
{
    /// <summary>
    /// Error network packet.
    /// </summary>
    [Serializable]
    public class ErrorPacket : NetworkPacket
    {
        public new ErrorBody Body { get; set; }
      
    }
}
