﻿using MSF.Network.Packet.Body;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet
{
    [Serializable]
    public class TerminalPacket : NetworkPacket
    {
        public new TerminalBody Body { get; set; }
    }
}
