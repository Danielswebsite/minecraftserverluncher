﻿using MSF.Network.Packet.Body;
using MSF.Network.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet
{
    /// <summary>
    /// Networkd packet there definie the app-protecole for the minecraft server luncher.
    /// </summary>
    [Serializable]
    public abstract class NetworkPacket 
    {    
        /// <summary>
        /// Add default headers.
        /// </summary>
        public NetworkPacket(HostToken hostToken = null)
        {
            Header = new Dictionary<string, string>();
            Header.Add("SoftwareVersion", "1.0.0");
            Header.Add("ProtecoleVersion", "1.0.0");
            Header.Add("PacketType", GetType().Name);

            if (hostToken != null)
                Header.Add("auth_token",hostToken.Value);

            Status = 1;
        }

        public bool HasHeaderKey(string key) 
        {
            return Header[key] != null;
        }

        /// <summary>
        /// Heder for the packet.
        /// </summary>
        public Dictionary<string, string> Header { get; protected set; }     
        
        /// <summary>
        /// The body for the network packet.
        /// </summary>
        public NetworkPacketBody Body { get; set; }
        
        /// <summary>
        /// Status code.
        /// </summary>
        public int Status { get; set; }
    }
}
