﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSF.Network.Packet.Body;

namespace MSF.Network.Packet
{
    [Serializable]
    public class AuthPacket : NetworkPacket
    {
        public AuthPacket() : base() 
        {
            
        }

        public new AuthBody Body { get; set; }
    }
}
