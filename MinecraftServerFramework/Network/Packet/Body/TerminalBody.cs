﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet.Body
{
    [Serializable]
    public class TerminalBody
    {
        public string TerminalOutput { get; set; }
        public string UUID { get; set; }
    }
}
