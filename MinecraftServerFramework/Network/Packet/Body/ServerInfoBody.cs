﻿using MSF.Network.Io;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using static System.Net.Mime.MediaTypeNames;

namespace MSF.Network.Packet.Body
{
    [Serializable]
    public class ServerInfoBody : NetworkPacketBody
    {        
        public string Name { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public bool IsStarted { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string UUID { get; set; }                        
    }
}
