﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet.Body
{
    /// <summary>
    /// Body for a network packet.
    /// </summary>
    [Serializable]
    public abstract class NetworkPacketBody
    {
        /// <summary>
        /// The type body can be set to.
        /// </summary>
        public enum BodyType
        {
            Text,
            Object,
            Dynamic,
            Number
        }

        /// <summary>
        /// BodyType there descipe what type the body are.
        /// </summary>
        public virtual BodyType Type { get; set; }
    }
}
