﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet.Body
{
    [Serializable]
    public class MessageBody : NetworkPacketBody
    {
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
