﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet.Body
{
    [Serializable]
    public class AuthBody : NetworkPacketBody
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public override BodyType Type => BodyType.Object;
    }
}
