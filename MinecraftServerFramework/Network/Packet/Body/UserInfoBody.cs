﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet.Body
{
    [Serializable]
    public class UserInfoBody : NetworkPacketBody
    {
        public string UUID { get; set; }
        public string UserName { get; set; }
        public string TokenValue { get; set; }
        public DateTime TokenExpiration { get; set; }
    }
}
