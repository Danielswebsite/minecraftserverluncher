﻿using MSF.Network.Packet.Body;
using MSF.Network.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet
{
    /// <summary>
    /// Packet information token.
    /// </summary>
    [Serializable]
    public class UserInformationPacket : NetworkPacket
    {
        public new UserInfoBody Body { get; set; }
    }
}
