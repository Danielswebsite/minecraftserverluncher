﻿using MSF.Network.Packet.Body;
using MSF.Network.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Packet
{
    [Serializable]
    public partial class MessagePacket : NetworkPacket
    {
        public MessagePacket() : base() 
        {
        
        }

        public MessagePacket(HostToken hostToken) : base(hostToken) 
        {
        
        }

        public new MessageBody Body { get; set; }
    }
}
