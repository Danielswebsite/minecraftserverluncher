﻿using MSF.Network.Packet;
using MSF.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network
{
    public class HostServer : Host
    {
        private TcpListener tcplistner;

        private IPAddress ipaddress;

        private int port;

        public ICollection<HostConnection> Connactions { get; private set; }

        public HostServer(string host, int port, byte maxconnactions = 10)
        {
            ipaddress = IPAddress.Parse(host);
            this.port = port;
            Connactions = new List<HostConnection>();
        }

        public async void StartAsync()
        {
            tcplistner = new TcpListener(ipaddress, port);
            tcplistner.Start();

            await Task.Run(() =>
            {
                while (true)
                {
                    Socket socket = tcplistner.AcceptSocket();                    
                    HandleSocketAsync(socket);
                }
            });
        }

        private HostConnection CreateConnection(Socket socket)
        {
            var connection = new HostConnection() { Socket = socket };
            Connactions.Add(connection);
            return connection;
        }

        public void RemoveConnection(HostConnection connection)
        {            
            Connactions.Remove(connection);            
        }

        private async void HandleSocketAsync(Socket socket)
        {            
            await Task.Run(() =>
            {
                var connection = CreateConnection(socket);
                OnConected(connection);
                
                while (IsConnected(connection))
                    OnResivePacket(connection);
                
                RemoveConnection(connection);
            });
        }

        public void Stop()
        {
            tcplistner.Stop();
        }
    }
}
