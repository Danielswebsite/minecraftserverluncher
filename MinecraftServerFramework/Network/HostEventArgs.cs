﻿using MSF.Network.Packet;
using MSF.Network.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network
{
    public class HostEventArgs : EventArgs
    {
        public HostEventArgs(NetworkPacket packet = null)
        {
            Packet = packet;
        }
        
        public NetworkPacket Packet { get; private set; }
    }
}
