﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Security.Resluts
{
    public class LoginResult : IResult
    {
        public HostUser User { get; set; }
        public bool IsLogin { get; set; }
    }
}
