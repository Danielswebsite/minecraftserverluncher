﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Security
{
    // https://www.c-sharpcorner.com/article/encryption-and-decryption-using-a-symmetric-key-in-c-sharp/
    // https://docs.microsoft.com/en-us/dotnet/standard/security/encrypting-data
    // https://stackoverflow.com/questions/6482883/how-to-generate-rijndael-key-and-iv-using-a-passphrase
    public class HostEncryption
    {
        /// <summary>
        /// Salt for this encryption
        /// </summary>
        private readonly string salt;

        /// <summary>
        /// Password for this encryption.
        /// </summary>
        private readonly string password;

        /// <summary>
        /// Instaciate new host encryption.
        /// </summary>
        public HostEncryption(string password, string salt)
        {
            this.password = password;
            this.salt = salt;
        }

        /// <summary>
        /// Decrypt bytes there are encrypted. Can be used to decrypt TCP socket data bytes there has a encryption to secure data over the internet.
        /// </summary>
        /// <param name="crypteddata"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public byte[] DecryptBytes(byte[] bytes)
        {
            Rfc2898DeriveBytes rdb = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(salt));
           
            using (AesManaged aes = new AesManaged())
            {
                aes.Key = rdb.GetBytes(32);
                aes.IV = rdb.GetBytes(16);
                aes.Padding = PaddingMode.PKCS7;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(bytes, 0, bytes.Length);
                    }
                    return memoryStream.ToArray();
                }
            }
        }

        /// <summary>
        /// Encrypt bytes. Can be used to encrypt TCP socket data bytes to secure data over the internet.
        /// </summary>
        /// <param name="crypteddata"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public byte[] EncryptBytes(byte[] bytes)
        {
            Rfc2898DeriveBytes rdb = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(salt));
            
            using (AesManaged aes = new AesManaged())
            {
                aes.Key = rdb.GetBytes(32);
                aes.IV = rdb.GetBytes(16);
                aes.Padding = PaddingMode.PKCS7;

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(bytes, 0, bytes.Length);
                    }
                    return memoryStream.ToArray();
                }
            }
        }
    }
}
