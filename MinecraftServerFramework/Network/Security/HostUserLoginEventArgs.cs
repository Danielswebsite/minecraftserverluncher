﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Security
{
    /// <summary>
    /// Event args for user login event.
    /// </summary>
    public class HostUserLoginEventArgs : EventArgs
    {

        /// <summary>
        /// Init new host args instance with connection.
        /// </summary>
        /// <param name="hostConnection"></param>
        public HostUserLoginEventArgs(HostConnection hostConnection) 
        {
            HostConnection = hostConnection;            
        }


        /// <summary>
        /// Connection for the user there  have login.
        /// </summary>
        public HostConnection HostConnection { get; protected set; }

        

    }
}
