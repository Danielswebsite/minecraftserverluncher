﻿using MSF.Network.Security.Storeges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Security.Maneger
{
    /// <summary>
    /// Mange the host userse.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class HostUserManager<U> where U : HostUser
    {
        private HostUserStoregeProvider<U> HostUserStoregeProvider;

        /// <summary>
        /// Int the hostusermanager with hostuserstorigeprovider
        /// </summary>
        public HostUserManager(HostUserStoregeProvider<U> hostUserStorgeProvider) 
        {
            this.HostUserStoregeProvider = hostUserStorgeProvider;
        }

        /// <summary>
        /// Signup and make user in user xml storge.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public void SignUp(string username, string password) 
        {            
            U user = Activator.CreateInstance<U>();
            user.UserName = username;
            user.HasedPassword = HostHassing.SHA256PasswordHassing(password);
            
            HostUserStoregeProvider.AddUser(user);
            HostUserStoregeProvider.SaveData();
        }  

        /// <summary>
        /// Check for is there on user or more in the user storege.
        /// </summary>
        /// <returns></returns>
        public bool HasUsers() 
        {
           return HostUserStoregeProvider.Users.Count() > 0;
        }

    }
}
