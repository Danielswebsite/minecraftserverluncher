﻿using MSF.Network.Packet;
using MSF.Network.Packet.Body;
using MSF.Network.Security.Resluts;
using MSF.Network.Security.Storeges;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MSF.Network.Security.Maneger
{
    // TODO: Make sign and culd not login event and metods for disconet if userinformation is wring. 

    /// <summary>
    /// Login manager for client siste.
    /// </summary>
    /// <typeparam name="U"></typeparam>
    public partial class HostLoginManeger<U> where U : HostUser
    {
        private HostClient hostClient;
        public HostLoginManeger(HostClient hostClient, HostConnection hostConnection)
        {
            this.hostClient = hostClient;
            this.hostConnection = hostConnection;
        }

        private void Resive_Event(object sender, HostEventArgs e)
        {            
            if (e.Packet is UserInformationPacket)
            {
                UserInformationPacket packet = (UserInformationPacket)e.Packet;
                HostUser hostUser = new HostUser(packet.Body);
                OnLogin(hostUser);
            }

            else if (e.Packet is ErrorPacket)
            {
                OnLoginFail(null);
            }

        }

        public void SendLoginRequest(string userName, string password)
        {
            AuthPacket authPacket = new AuthPacket();
            authPacket.Body = new AuthBody() { Password = password, UserName = userName };
            hostClient.SendPacket(authPacket);
            hostClient.ResivePacketEvent += Resive_Event;
        }
    }

    /// <summary>
    /// Login manager for server site.
    /// </summary>
    /// <typeparam name="U"></typeparam>
    public partial class HostLoginManeger<U> where U : HostUser
    {
        private HostConnection hostConnection;
        private HostUserStoregeProvider<U> hostUserStoregeProvider;
        private HostServer hostServer;

        public HostLoginManeger(HostUserStoregeProvider<U> hostUserStoregeProvider, HostServer hostServer, HostConnection connection)
        {
            this.hostServer = hostServer;
            this.hostConnection = connection;
            this.hostUserStoregeProvider = hostUserStoregeProvider;
        }

        public delegate void LoginEventHandler(object sender, HostUserLoginEventArgs e);

        public event LoginEventHandler LoginEvent;

        /// <summary>
        /// Call login event with sender as host user
        /// </summary>
        /// <param name="hostUser"></param>
        private void OnLogin(HostUser hostUser)
        {
            LoginEvent?.Invoke(hostUser, new HostUserLoginEventArgs(hostConnection));
        }

        public delegate void LoginFailEventHandler(object sender, HostUserLoginFailEventArgs e);

        public event LoginFailEventHandler LoginFailEvent;

        private void OnLoginFail(HostUser hostUser)
        {
            LoginFailEvent?.Invoke(hostUser, new HostUserLoginFailEventArgs(hostConnection, "Fail could not login!"));
        }

        /// <summary>
        /// Login a user in with a username and password.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public LoginResult Login(string username, string password)
        {
            string hasedPassword = HostHassing.SHA256PasswordHassing(password);

            if ((hostUserStoregeProvider.Users as List<HostUser>).Exists(i => i.UserName.Equals(username) && i.HasedPassword.Equals(hasedPassword)))
            {
                U user = Activator.CreateInstance<U>();

                hostConnection.Token = new HostToken(user);
                hostConnection.Token.ReleaseToken(DateTime.Now.AddDays(1));

                MessagePacket packet = new MessagePacket();

                OnLogin(user);

                UserInformationPacket userInformationPacket = new UserInformationPacket();
                userInformationPacket.Status = 01;
                userInformationPacket.Body = new UserInfoBody
                {
                    UUID = user.UUID,
                    UserName = user.UserName,
                    TokenValue = user.Token.Value,
                    TokenExpiration = user.Token.Expiration
                };

                hostServer.SendPacket(hostConnection, userInformationPacket);

                return new LoginResult { IsLogin = true, User = user };
            }


            else
            {
                ErrorPacket errorPacket = new ErrorPacket();
                errorPacket.Body = new ErrorBody { Type = NetworkPacketBody.BodyType.Object, Message = "Wrong login informations." };
                
                hostServer.SendPacket(hostConnection, errorPacket);

                return new LoginResult { IsLogin = false };
            }

        }
    }
}
