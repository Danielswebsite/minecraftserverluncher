﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

// Ref DOC: http://geekswithblogs.net/Nettuce/archive/2012/06/14/salt-and-hash-a-password-in.net.aspx
namespace MSF.Network.Security
{
    /// <summary>
    /// Hasing for password and userdata
    /// </summary>
    public class HostHassing : IDisposable
    {
        /// <summary>
        /// Salt to secure the hashed password so it not the same hash for the same plain text password.
        /// </summary>
        public string Salt { get; protected set; }

        /// <summary>
        /// The hashed password.
        /// </summary>
        public string Hash { get; protected set; }

        private string password;

        /// <summary>
        /// Hassing with and without salt.
        /// </summary>
        public HostHassing(string password)
        {
            this.password = password;
        }

        private HostHassing(string password, HashAlgorithmName hashAlgorithmName)
        {
            this.password = password;            
            ComputeSalt();
            ComputeHash(Salt, hashAlgorithmName);
        }

        /// <summary>
        /// Init new object og the HostHasing and compute the hash of a password.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="hashAlgorithmName"></param>
        /// <returns></returns>
        public static HostHassing NewHash(string password, HashAlgorithmName hashAlgorithmName)
        {
            return new HostHassing(password, hashAlgorithmName);
        }


        /// <summary>
        /// Make salt to hash the password.
        /// </summary>
        public void ComputeSalt()
        {
            byte[] saltBytes = new byte[32];

            using (RNGCryptoServiceProvider serviceProvider = new RNGCryptoServiceProvider())
                serviceProvider.GetNonZeroBytes(saltBytes);

            Salt = Convert.ToBase64String(saltBytes);
        }

        /// <summary>
        /// Hash the password with specfied salt.
        /// </summary>
        public void ComputeHash(string salt, HashAlgorithmName hashAlgorithmName)
        {
            byte[] saltedBytes = Convert.FromBase64String(salt);

            using (Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(password, saltedBytes, 1000, hashAlgorithmName))
                Hash = Convert.ToBase64String(rfc.GetBytes(256));
        }

        /// <summary>
        /// Hash the password with the computed salt.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="hashAlgorithmName"></param>
        public void ComputeHash(HashAlgorithmName hashAlgorithmName) 
        {
            if (Hash is null)
                throw new Exception("The 'ComputeSalt is not called to computed a salt for hassing the password.'");

            ComputeHash(Hash, hashAlgorithmName);
        }

        /// <summary>
        /// Vertify the hash is the same as from the plain text.
        /// </summary>
        /// <returns></returns>
        public static bool VertifyHash(string password, string hash, string salt, HashAlgorithmName hashAlgorithmName)
        {
            using (HostHassing hostHassing = new HostHassing(password))
            {
                hostHassing.ComputeHash(salt, hashAlgorithmName);
                return hash.Equals(hostHassing.Hash);
            }
        }

        /// <summary>
        /// Has a password in SAH256 alguritme.
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        [Obsolete("Use instate a object of this class to make hassing.")]
        public static string SHA256PasswordHassing(string password)
        {
            StringBuilder passwordStringBuilder = new StringBuilder();

            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hasBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));

                foreach (var by in hasBytes)
                    passwordStringBuilder.Append(by.ToString("x2"));
            }
            return passwordStringBuilder.ToString();
        }

        public void Dispose()
        {
            Salt = null;
            Hash = null;
            GC.Collect();
            GC.SuppressFinalize(this);
        }
    }
}
