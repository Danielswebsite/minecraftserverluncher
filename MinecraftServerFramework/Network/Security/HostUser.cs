﻿using MSF.Network.Packet.Body;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MSF.Network.Security
{
    /// <summary>
    /// Host user for thw host network protocole.
    /// </summary>
    [Serializable]
    [XmlRoot(Namespace = "msf", ElementName = "user")]
    public class HostUser
    {
        /// <summary>
        /// Host user for auth on host.
        /// </summary>        
        public HostUser()
        {
            UUID = $"{ Guid.NewGuid() }{ DateTime.Now.ToString("yyyMMddhhmmss") }";
        }

        public HostUser(UserInfoBody body)
        {
            UUID = body.UUID;
            UserName = body.UserName;
            Token = new HostToken(user: this, expiration: body.TokenExpiration, value: body.TokenValue);
        }

        [XmlElement]
        public string UUID { get; set; }

        [XmlElement]
        public string UserName { get; set; }
        [XmlElement]
        public string HasedPassword { get; set; }

        [XmlIgnore]
        public HostToken Token { get; set; }

    }
}
