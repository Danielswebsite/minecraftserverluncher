﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Security
{
    public class HostUserLoginFailEventArgs : EventArgs
    {
        public HostUserLoginFailEventArgs(HostConnection hostConnection, string errorMessage) 
        {
            HostConnection = hostConnection;
            ErrorMessage = errorMessage;
        }
        public HostConnection HostConnection { get; protected set; }
        public string ErrorMessage { get; protected set; }
    }
}
