﻿using MSF.Network.Packet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Security
{
    public class HostAutocation
    {
        protected HostConnection Connection { get; set; }

        private static ICollection<HostAutocation> hostAutocations = new List<HostAutocation>();

        public static HostAutocation GetAutocation(HostConnection connection)
        {
            HostAutocation hostAutocation;

            if (connection.Token != null)
            {
                hostAutocation = hostAutocations.FirstOrDefault(i => i.Connection.Token.Equals(connection.Token));

                if (hostAutocation != null)
                    return hostAutocation;                
            }

            hostAutocation = new HostAutocation(connection);
            hostAutocations.Add(hostAutocation);
            return hostAutocation;
        }

        public HostAutocation(HostConnection connection)
        {
            this.Connection = connection;
        }

        /// <summary>
        /// Check for is user are autocatet
        /// </summary>
        /// <returns></returns>
        public bool IsPacketTokenAutocatet(NetworkPacket networkPacket)
        {
            if (Connection.Token is null)
                return false;

            if (!networkPacket.Header.ContainsKey("auth_token"))
                return false;

            string authHeader = networkPacket.Header["auth_token"];

            if (authHeader == string.Empty)
                return false;

            return (!Connection.Token.IsExpired()) && networkPacket.Header["auth_token"].Equals(Connection.Token.ToString());
        }

    }
}
