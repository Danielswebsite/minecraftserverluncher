﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Security
{    
    /// <summary>
    /// Host token for a host user.
    /// </summary>
    public class HostToken 
    {
        /// <summary>
        /// Make new value for token and expiration date.
        /// </summary>
        /// <param name="Expiration"></param>
        /// <returns></returns>
        public void ReleaseToken(DateTime expiration) 
        {           
            Value = Guid.NewGuid().ToString();
            Expiration = expiration;        
        }

        /// <summary>
        /// Iinstaciate host token with user.
        /// </summary>
        /// <param name="user"></param>
        public HostToken(HostUser user, DateTime? expiration = null, string value = null) 
        {
            User = user;
            User.Token = this;

            if (expiration != null)
                Expiration = expiration.Value;

            if(value != null)
                Value = value;           
        }

        /// <summary>
        /// Expired the token date.
        /// </summary>
        public void Expired() 
        {
            Expiration = DateTime.Now;
        }

        /// <summary>
        /// Check for is the tokens expired date are expired.
        /// </summary>
        /// <returns></returns>
        public bool IsExpired() 
        {
            return Expiration <= DateTime.Now;
        }

        public HostUser User { get; private set; }

        public string Value { get; private set; }
        public DateTime Expiration { get; private set; }

        /// <summary>
        /// Get value of the token as string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Value;
        }

    }
}
