﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MSF.Network.Security.Storeges
{

    /// <summary>
    /// Host storge privder to storge user information.
    /// </summary>
    public class HostUserStoregeProvider<U> where U : HostUser
    {
        public List<U> Users { get; set; }

        private FileStream fileStream;
        private XmlSerializer xmlSerializer;
        private FileInfo fileInfo;

        /// <summary>
        /// Instaciate a new storge user information from a xml file.
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public HostUserStoregeProvider(FileInfo fileInfo)
        {
            xmlSerializer = new XmlSerializer(typeof(List<U>));
            this.fileInfo = fileInfo;

            if (fileInfo.Exists)
                LoadData();
            else
                SaveData();
        }

        /// <summary>
        /// Add a host user to storge
        /// </summary>
        /// <param name=""></param>
        public void AddUser(U user)
        {
            Users.Add(user);
        }

        /// <summary>
        /// Read data from user xml file.
        /// </summary>
        public void LoadData()
        {
            using (fileStream = fileInfo.OpenRead())
                Users = (List<U>)xmlSerializer.Deserialize(fileStream);
        }

        /// <summary>
        /// Write userdata to xml file.
        /// </summary>
        public void SaveData()
        {
            if (Users is null)
                Users = new List<U>();

            using (fileStream = fileInfo.OpenWrite())
                xmlSerializer.Serialize(fileStream, Users);
        }
    }
}
