﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSF.Server;
using MSF.Network.Packet;
using MSF.Network.Packet.Body;
using System.Net;
using MSF.Network.Security;
using MSF.Network.Io;

namespace MSF.Network.Minecarft
{


    /// <summary>
    /// Get information for a specifik server on a client.
    /// </summary>
    public partial class MinecraftServerHostInformationProvider
    {
        private readonly HostClient hostClient;

        private readonly HostToken hostToken;

        public MinecraftServerHostInformationProvider(HostClient hostClient, HostToken hostToken = null)
        {
            this.hostClient = hostClient;

            if (hostToken != null)
                this.hostToken = hostToken;

            hostClient.ResivePacketEvent += PacketResicve_Event;
        }

        public delegate void ResiveServerHandler(object sender, HostResiveServerEventArgs e);

        public event ResiveServerHandler ResiveServerEvent;

        private void OnResiveServer(MinecraftHostServer minecraftHostServer)
        {
            ResiveServerEvent?.Invoke(null, new HostResiveServerEventArgs(minecraftHostServer));
        }

        /// <summary>
        /// Send a packet with a request to resend packets with server informations.
        /// </summary>
        public void RequestServerList()
        {
            MessagePacket messagePacket = new MessagePacket(hostToken);

            messagePacket.Header["action"] = "get-servers";

            messagePacket.Body = new MessageBody
            {
                Title = "Test",
                Message = "Test"
            };

            hostClient.SendPacket(messagePacket);
        }

        private void PacketResicve_Event(object sender, HostEventArgs e)
        {
            if (e.Packet is ServerInforPacket)
            {
                if (e.Packet.HasHeaderKey("action"))
                {
                    if (e.Packet.Header["action"].Equals("add-server"))
                    {
                        ServerInforPacket serverInforPacket = (ServerInforPacket)e.Packet;
                        OnResiveServer(new MinecraftHostServer(serverInforPacket, hostClient));
                    }
                }
            }
        }
    }

    /// <summary>
    /// Get information for a specifik server on a host.
    /// </summary>
    public partial class MinecraftServerHostInformationProvider
    {
        private HostConnection hostConnection;
        private HostServer hostServer;
        private HostAutocation hostAutocation;
        private ICollection<MinecraftServer> minecraftServers;

        public MinecraftServerHostInformationProvider(ICollection<MinecraftServer> minecraftServers, HostConnection hostConnection, HostServer hostServer, HostAutocation hostAutocation = null)
        {
            this.hostConnection = hostConnection;
            this.hostServer = hostServer;
            this.minecraftServers = minecraftServers;
            this.hostAutocation = hostAutocation;

            hostServer.ResivePacketEvent += Resive_event;

            foreach (MinecraftServer minecraftServer in minecraftServers)
                minecraftServer.TerminalEvent += Terminal_Event;
        }

        private void Resive_event(object sender, HostEventArgs e)
        {
            if (hostAutocation is null ^ hostAutocation.IsPacketTokenAutocatet(e.Packet))
            {
                if (e.Packet is MessagePacket)
                {
                    MessageBody messageBody = ((MessagePacket)e.Packet).Body;

                    if (e.Packet.HasHeaderKey("action"))
                    {
                        if (e.Packet.Header["action"].Equals("get-servers"))
                        {
                            SendServerList();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Send the terminal output line to client in a packet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Terminal_Event(MinecraftServer sender, string e)
        {
            hostServer.SendPacket(hostConnection, new TerminalPacket() { Body = new TerminalBody() { UUID = sender.UUID, TerminalOutput = e } });
        }

        /// <summary>
        /// Send virtuel server informations to clients.
        /// </summary>
        public void SendServerList()
        {
            ServerInforPacket serverInforPacket = new ServerInforPacket();

            serverInforPacket.Header.Add("action", "add-server");

            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (MinecraftServer minecraftServer in minecraftServers)
            {
                MinecraftServerProperties minecraftServerProperties = minecraftServer.GetProperties();

                serverInforPacket.Body = new ServerInfoBody()
                {
                    UUID = minecraftServer.UUID,
                    Name = minecraftServer.GetServerFolderName(),
                    FileName = minecraftServer.GetServerFileName(),
                    IsStarted = minecraftServer.JavaServices.IsRunning(),
                    Path = minecraftServer.ServerPath,
                    Host = host.AddressList.FirstOrDefault(i => !i.IsIPv6LinkLocal).ToString(),
                    Port = minecraftServerProperties.ServerPort,                    
                    Type = NetworkPacketBody.BodyType.Object
                };

                hostServer.SendPacket(hostConnection, serverInforPacket);
            }
        }
    }
}
