﻿using MSF.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Minecarft
{
    public class HostResiveServerEventArgs : EventArgs
    {
        public HostResiveServerEventArgs(MinecraftHostServer minecraftHostServer) 
        {
            HostServer = minecraftHostServer;
        }

        public MinecraftHostServer HostServer { get; private set; }
    }
}
