﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network.Io
{
    [Serializable]
    public abstract class HostFile
    {
        public byte[] Data { get; set; }
    }
}
