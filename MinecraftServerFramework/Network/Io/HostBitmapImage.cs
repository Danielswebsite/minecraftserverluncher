﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MSF.Network.Io
{
    /// <summary>
    /// Host image to be sente with the tcp network protecole.
    /// </summary>    
    [Serializable]
    public class HostBitmapImage : HostFile
    {        
        /// <summary>
        /// Init the HostBitmapImage with a normail BitmapImage.
        /// </summary>
        /// <param name="image"></param>
        public HostBitmapImage(BitmapImage image) 
        {
            Data = ToByteArray(image);
        }

        public HostBitmapImage(string path) 
        {
            var image = new BitmapImage(new Uri(path));
            Data = ToByteArray(image);
        }
        
        /// <summary>
        /// Get bitmapimage from hostbitmap image.
        /// </summary>
        /// <returns></returns>        
        public Bitmap ToBitmapImage() 
        {
            return ToBitmapImage(Data);
        }

        /// <summary>
        /// Converte the image to bitmapimage.
        /// </summary>
        /// <returns></returns>
        public static Bitmap ToBitmapImage(byte[] data) 
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream(data))
                return (Bitmap)binaryFormatter.Deserialize(memoryStream);               
        }

        /// <summary>
        /// To bytearray.
        /// </summary>
        /// <returns></returns>
        private static byte[] ToByteArray(BitmapImage image) 
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BitmapEncoder bitmapEncoder = new BmpBitmapEncoder();
                bitmapEncoder.Frames.Add(BitmapFrame.Create(image));
                bitmapEncoder.Save(memoryStream);

                byte[] data = memoryStream.ToArray();
                return data;
            }        
        }
    }
}
