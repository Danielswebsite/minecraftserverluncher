﻿using MSF.Network.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network
{
    public class HostConnection
    {
        public Socket Socket { get; set; }        
        public HostToken Token { get; set; }
    }
}
