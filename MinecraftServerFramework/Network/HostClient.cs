﻿using MSF.Network.Packet;
using MSF.Network.Packet.Body;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network
{
    /// <summary>
    /// Client to connect to hostserver.
    /// </summary>
    public class HostClient : Host 
    {        
        /// <summary>
        /// Connection to hostserver.
        /// </summary>
        private HostConnection connection;

        /// <summary>
        /// Send a network packet.
        /// </summary>
        /// <param name="packet"></param>
        public void SendPacket(NetworkPacket packet) => SendPacket(connection, packet);

        /// <summary>
        /// Connectio async to host server with a host name otr ip and port.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        public async void ConnectAsync(string host, int port)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(host, port);

            connection = new HostConnection() { Socket = socket };

            OnConected(connection);

            await Task.Run(() =>
            {
                while (connection.Socket.Connected)
                {
                    OnResivePacket(connection);
                }
            });
        }

        public void Disconect() => Disconect(connection);
    }
}
