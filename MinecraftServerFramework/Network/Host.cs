﻿using MSF.Network.Packet;
using MSF.Network.Packet.Body;
using MSF.Network.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Network
{
    /// <summary>
    /// Host for server and client.
    /// </summary>
    public abstract class Host
    {
        /// <summary>
        /// Get the default ip for this host.
        /// </summary>
        protected string DefaultAddress { get { return "127.0.0.1"; } }

        /// <summary>
        /// Get default port for this host type.
        /// </summary>
        protected int DefaultPort { get { return 9855; } }

        public delegate void HostServerEventHandler(object sender, HostEventArgs e);

        public event HostServerEventHandler ResivePacketEvent;

        public event HostServerEventHandler DisconectedEvent;

        public event HostServerEventHandler ConnectedEvent;

        public void SendPacket(HostConnection connection, NetworkPacket packet)
        {
            BinaryFormatter binary = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                binary.Serialize(stream, packet);
                byte[] data = stream.ToArray();
                connection.Socket.Send(data);
            }
        }

        /// <summary>
        /// Disconet or and release the resorse the socket connection.
        /// </summary>
        /// <param name="socket"></param>
        public void Disconect(HostConnection connection)
        {
            if (IsConnected(connection))
                connection.Socket.Disconnect(false);
            
            connection.Socket.Close();
            OnDisconcted(connection);
        }

        /// <summary>
        /// Trick the conect event.
        /// </summary>
        protected virtual void OnConected(HostConnection connection)
        {
            ConnectedEvent?.Invoke(connection, new HostEventArgs());
        }

        /// <summary>
        /// Trick the On disconedt event.
        /// </summary>
        /// <param name="connection"></param>
        protected virtual void OnDisconcted(HostConnection connection)
        {
            DisconectedEvent?.Invoke(connection, new HostEventArgs());
        }

        /// <summary>
        /// Check for is the connection are conedted.
        /// </summary>
        /// <returns></returns>
        protected bool IsConnected(HostConnection connection)
        {
            try
            {
                if (connection.Socket.Connected)
                    return !(connection.Socket.Poll(1, SelectMode.SelectRead) && connection.Socket.Available.Equals(0));

                return false;
            }
            catch (SocketException)
            {
                return false;
            }
        }

        protected void OnResivePacket(HostConnection connection)
        {
            byte[] data = new byte[connection.Socket.ReceiveBufferSize];

            connection.Socket.Receive(data);
            BinaryFormatter binary = new BinaryFormatter();

            using (MemoryStream stream = new MemoryStream(data))
            {
                if (stream.Length > 0)
                {
                    try
                    {
                        var packet = binary.Deserialize(stream);
                        if (packet is NetworkPacket)
                        {
                            NetworkPacket networkPacket = (NetworkPacket)packet;
                            ResivePacketEvent?.Invoke(connection, new HostEventArgs(networkPacket));
                        }
                    }
                    catch (Exception e)
                    {
                        return;
                    }
                }
            }
        }
    }
}
