﻿using MSF.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Version
{
    public class MinecraftVersion
    {
        private static MinecraftVersion version;

        private MinecraftVersion(MinecraftServer server)
        {
            try
            {
                string[] namesekments = server.GetServerFileName().ToLower().Replace("spigot-", string.Empty).Replace(".jar", string.Empty).Split('.');
                Major = int.Parse(namesekments[0]);
                Minor = int.Parse(namesekments[1]);
                Patch = int.Parse(namesekments[2]);
            }
            catch (Exception)
            {
                throw new Exception("The name of the minecraft server file shult contain a name like spigot-1.14.2 or bukkit-1.10.2!");
            }
        }

        public static MinecraftVersion GetVersion(MinecraftServer server)
        {
            if (version == null)
            {
                version = new MinecraftVersion(server);
            }
            return version;
        }

        public int Major { get; set; }
        public int Minor { get; set; }
        public int Patch { get; set; }
    }
}
