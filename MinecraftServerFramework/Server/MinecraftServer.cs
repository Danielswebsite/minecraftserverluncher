﻿using MSF.Java;
using MSF.Player;
using MSF.Plugin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MSF.Server
{

    [Serializable]
    public sealed class MinecraftServer : IServer
    {
        public MinecraftServer()
        {
            JavaServices = new JavaExicuter();
            UUID = Guid.NewGuid().ToString();
        }

        public string UUID { get; private set; }

        private string proppath => $"{ServerPath}/server.properties";

        public string ServerIconPath => $@"{ServerPath}\server-icon.png";

        public int MinMemery { get; set; }

        public JavaExicuter JavaServices { get; set; }

        public int MaxMemery { get; set; }

        public string GetServerFolderName()
        {
            return ServerPath.Split('\\').Last();
        }

        public MinecraftServerDirectory ServerDirectory { get; set; }

        public string SetServerFolderName(MinecraftServerDirectory dir, string name)
        {
            string newfolderpath = $"{dir.Path}\\{ name }";

            if (!Directory.Exists(newfolderpath))
            {
                Directory.Move(ServerPath, newfolderpath);
                ServerPath = newfolderpath;
            }

            return ServerPath;
        }

        public List<MinecraftPlugin> Plugins { get; set; }

        public List<MinecraftPlugin> LoadPlugins()
        {
            string path = $"{ ServerPath }\\plugins";

            Plugins = new List<MinecraftPlugin>();

            if (Directory.Exists(path))
            {
                foreach (var pluginpath in Directory.GetFiles(path))
                {
                    Plugins.Add(new MinecraftPlugin() { PluginFolderPath = path, PluginPath = pluginpath });
                }
            }
            return Plugins;
        }

        public List<MinecraftPlayer> GetWhiteListPlayers()
        {
            var players = new List<MinecraftPlayer>();

            string path = $"{ ServerPath }\\whitelist.json";

            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader reader = File.OpenText(path))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        players = (List<MinecraftPlayer>)serializer.Deserialize(reader, typeof(List<MinecraftPlayer>));
                    }
                }
            }
            catch (Exception)
            {

            }

            return players;
        }

        public void AddPlayerToWhiteList(MinecraftPlayer player)
        {
            SendCommand($"whitelist add {player.Name}");
        }

        public void RemovePlayerFromWhiteList(MinecraftPlayer player)
        {
            SendCommand($"whitelist remove {player.Name}");
        }

        public void Remove()
        {
            if (Directory.Exists(ServerPath))
            {
                var dir = new DirectoryInfo(ServerPath);
                dir.Attributes = FileAttributes.Normal;
                dir.Delete(true);
            }
        }

        public void ShowLogInNotePad()
        {
            Process.Start("notepad.exe", $"{ ServerPath }\\logs\\latest.log");
        }

        public void RemovePlugin(MinecraftPlugin plugin)
        {
            File.Delete(plugin.PluginPath);
        }

        public MinecraftPlugin AddPlugin(string path)
        {
            var plugin = new MinecraftPlugin();

            string pluginfolderpath = $"{ServerPath}\\plugins\\";

            string pluginfilepath = $"{pluginfolderpath}{path.Split('\\').Last()}";

            int index = 1;

            string[] pathdata = path.Split('\\').Last().Split('.');

            while (true)
            {
                if (File.Exists(pluginfilepath))
                {
                    if (pathdata.Length > 2)
                    {
                        pluginfilepath = pluginfolderpath;

                        for (int i = 0; i < pathdata.Length - 1; i++)
                            pluginfilepath += pathdata[i];

                        pluginfilepath += $"({index}).{pathdata.Last()}";
                    }

                    else
                        pluginfilepath = $"{pluginfolderpath}{pathdata[0]}({index}).{pathdata.Last()}";
                }
                else
                    break;

                index++;
            }

            File.Copy(path, pluginfilepath);
            plugin.PluginFolderPath = pluginfolderpath;
            plugin.PluginPath = pluginfilepath;

            return plugin;
        }

        public string GetServerFileName()
        {
            return Directory.GetFiles(ServerPath).First(i => i.Contains(".jar"));
        }

        public void OpPlayer(MinecraftPlayer player)
        {
            var ops = GetOps();

            if (ops.Exists(i => i.Name == player.Name))
                return;

            SendCommand($"op {player.Name}");
        }

        public void DeOpPlayer(MinecraftPlayer player)
        {
            SendCommand($"deop { player.Name }");
        }

        public List<MinecraftPlayer> GetOps()
        {
            string path = $"{ ServerPath }\\ops.json";

            if (File.Exists(path))
            {
                using (StreamReader reader = File.OpenText(path))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    return (List<MinecraftPlayer>)serializer.Deserialize(reader, typeof(List<MinecraftPlayer>));
                }
            }

            else
            {
                return new List<MinecraftPlayer>();
            }
        }

        public void RemoveIcon()
        {
            if (File.Exists(ServerIconPath))
                File.Delete(ServerIconPath);
        }

        public void SetServerIcon(string iconpath)
        {
            if (File.Exists(ServerIconPath))
                File.Delete(ServerIconPath);

            File.Copy(iconpath, ServerIconPath);
        }

        public string ServerPath { get; set; }

        public bool HasProperties()
        {
            if (!File.Exists(proppath))
                return false;

            IOException exception = new IOException();

            // Fix this later!
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    int numberoflines = File.ReadAllLines(proppath).Length;
                    return (numberoflines > 3);
                }
                catch (IOException e)
                {
                    exception = e;
                    Thread.Sleep(200);
                }
            }

            throw exception;

        }

        public void SendCommand(string command)
        {
            JavaServices.SendText(command);
        }

        public MinecraftServerProperties GetProperties()
        {
            MinecraftServerProperties properties = new MinecraftServerProperties();

            if (HasProperties())
                try
                {
                    string workcopyproppath = $"{ proppath.Split('.')[0] }_workcopy.{ proppath.Split('.')[1] }";

                    if (File.Exists(workcopyproppath))
                        File.Delete(workcopyproppath);

                    File.Copy(proppath, workcopyproppath);
                    var lines = File.ReadAllLines(workcopyproppath);
                    File.Delete(workcopyproppath);
                    for (int i = 0; i < lines.Length; i++)
                    {
                        string[] line = lines[i].Split('=');

                        if (line[0].Equals("spawn-protection"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnProtection = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("max-tick-time"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxTickTime = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("generator-settings"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.GeneratorSettings = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("force-gamemode"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ForceGamemode = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("allow-nether"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.AllowNether = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("gamemode"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Gamemode = (GameMode)Enum.Parse(typeof(GameMode), line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("broadcast-console-to-ops"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.BroadcastConsoleToOps = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("enable-query"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.EnableQuery = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("player-idle-timeout"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.PlayerIdleTimeout = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("difficulty"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Difficulty = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("spawn-monsters"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnMonsters = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("op-permission-level"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.OpPermissionLevel = byte.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("pvp"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Pvp = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("snooper-enabled"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SnooperEnabled = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("level-type"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.LevelType = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("hardcore"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Hardcore = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("enable-command-block"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.EnableCommandBlock = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("max-players"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxPlayers = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("network-compression-threshold"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.NetworkCompressionThreshold = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("resource-pack-sha1"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ResourcePackSha1 = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("max-world-size"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxWorldSize = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("server-port"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ServerPort = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("server-ip"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ServerIp = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("spawn-npcs"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnNpcs = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("allow-flight"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.AllowFlight = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("level-name"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.LevelName = line[1];
                                }
                            }
                        }

                        if (line[0].Equals("view-distance"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ViewDistance = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("resource-pack"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.ResourcePack = line[1];
                                }

                            }
                        }

                        if (line[0].Equals("spawn-animals"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.SpawnAnimals = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("white-list"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.WhiteList = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("generate-structures"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.GenerateStructures = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("online-mode"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.OnlineMode = bool.Parse(line[1]);
                                }

                            }
                        }

                        if (line[0].Equals("max-build-height"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.MaxBuildHeight = uint.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("level-seed"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.LevelSeed = long.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("prevent-proxy-connections"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.PreventProxyConnections = bool.Parse(line[1]);
                                }

                            }
                        }

                        if (line[0].Equals("enable-rcon"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.EnableRcon = bool.Parse(line[1]);
                                }
                            }
                        }

                        if (line[0].Equals("motd"))
                        {
                            if (line.Length == 2)
                            {
                                if (line[1] != "")
                                {
                                    properties.Motd = line[1];
                                }
                            }
                        }

                    }
                }
                catch (Exception)
                {

                }

            return properties;
        }

        public void SetProperties(MinecraftServerProperties properties)
        {
            string proppath = $"{ServerPath}/server.properties";

            try
            {
                using (StreamWriter writer = new StreamWriter(proppath))
                {
                    writer.WriteLine($"enable-jmx-monitoring=false");
                    writer.WriteLine($"level-seed={properties.LevelSeed}");
                    writer.WriteLine($"rcon.port=25575");
                    writer.WriteLine($"enable-command-block={properties.EnableCommandBlock}");
                    writer.WriteLine($"gamemode={ Enum.GetName(typeof(GameMode),properties.Gamemode).ToLower() }");
                    writer.WriteLine($"enable-query={properties.EnableQuery}");
                    writer.WriteLine($"generator-settings={properties.GeneratorSettings}");
                    writer.WriteLine($"level-name={properties.LevelName}");
                    writer.WriteLine($"motd={properties.Motd}");
                    writer.WriteLine($"pvp={properties.Pvp}");
                    writer.WriteLine($"generate-structures={properties.GenerateStructures}");
                    writer.WriteLine($"difficulty={properties.Difficulty}");
                    writer.WriteLine($"network-compression-threshold={properties.NetworkCompressionThreshold}");
                    writer.WriteLine($"max-tick-time={properties.MaxTickTime}");
                    writer.WriteLine($"require-resource-pack=false");
                    writer.WriteLine($"max-players={properties.MaxPlayers}");
                    writer.WriteLine($"use-native-transport=true");
                    writer.WriteLine($"online-mode={properties.OnlineMode}");
                    writer.WriteLine($"enable-status=true");
                    writer.WriteLine($"allow-flight={properties.AllowFlight}");
                    writer.WriteLine("$broadcast-rcon-to-ops=true");
                    writer.WriteLine($"view-distance={properties.ViewDistance}");
                    writer.WriteLine($"max-build-height={properties.MaxBuildHeight}");
                    writer.WriteLine($"server-ip={properties.ServerIp}");
                    writer.WriteLine($"allow-nether={properties.AllowNether}");
                    writer.WriteLine($"server-port={properties.ServerPort}");
                    writer.WriteLine($"enable-rcon={properties.EnableRcon}");
                    writer.WriteLine($"sync-chunk-writes=true");
                    writer.WriteLine($"resource-pack-prompt=");
                    writer.WriteLine($"op-permission-level={ properties.OpPermissionLevel }");
                    writer.WriteLine($"prevent-proxy-connections={properties.PreventProxyConnections}");
                    writer.WriteLine($"hide-online-players=false");
                    writer.WriteLine($"resource-pack={properties.ResourcePack}");
                    writer.WriteLine($"entity-broadcast-range-percentage=100");
                    writer.WriteLine($"simulation-distance=10");
                    writer.WriteLine($"player-idle-timeout={properties.PlayerIdleTimeout}");
                    writer.WriteLine($"rcon.password=");
                    writer.WriteLine($"force-gamemode={properties.ForceGamemode}");
                    writer.WriteLine("$debug=false");
                    writer.WriteLine($"resource-pack-sha1={properties.ResourcePackSha1}");
                    writer.WriteLine($"broadcast-console-to-ops={properties.BroadcastConsoleToOps}");
                    writer.WriteLine($"spawn-monsters={properties.SpawnMonsters}");
                    writer.WriteLine($"rate-limit=0");
                    writer.WriteLine($"snooper-enabled={properties.SnooperEnabled}");
                    writer.WriteLine($"level-type={properties.LevelType}");
                    writer.WriteLine($"hardcore={properties.Hardcore}");
                    writer.WriteLine($"white-list={properties.WhiteList}");
                    writer.WriteLine($"max-world-size={properties.MaxWorldSize}");
                    writer.WriteLine($"spawn-animals={properties.SpawnAnimals}");
                    writer.WriteLine($"spawn-npcs={properties.SpawnNpcs}");
                    writer.WriteLine($"function-permission-level=2");
                    writer.WriteLine($"text-filtering-config=");
                    writer.WriteLine($"enforce-whitelist");
                    writer.WriteLine($"spawn-protection={properties.SpawnProtection}");

                }
            }

            catch (Exception)
            {

            }
        }

        public bool HasBinStartedBefore()
        {
            return !(Directory.GetDirectories(ServerPath).Length <= 1 && Directory.GetFiles(ServerPath).Length <= 3);
        }

        public bool IsDone { get; private set; }

        public void Start()
        {
            JavaServices.Args = new string[] { $"-Xms{ MinMemery }M", $"-Xmx{ MaxMemery }M", "-jar", $"{ GetServerFileName()}" };
            JavaServices.WorkingFolder = ServerPath;
            JavaServices.Start();
        }

        public delegate void ErrorExceptionHandler(Exception e);

        public event ErrorExceptionHandler StartExceptionEvent;

        public Exception Exception { get; private set; }

        private void OnStartException(Exception e)
        {
            this.Exception = e;
            StartExceptionEvent?.Invoke(e);
        }

        public delegate void TerminalHandler(MinecraftServer server, string line);

        public event TerminalHandler TerminalEvent;

        private void OnTerminal(MinecraftServer server, string line)
        {
            TerminalEvent?.Invoke(server, line);
        }

        public async Task StartAsync()
        {
            await Task.Run(() =>
            {
                try
                {
                    Start();
                }
                catch (Exception e)
                {
                    OnStartException(e);
                }
            });
        }

        public void EnableTerminalEvent()
        {
            while (JavaServices.Process == null && !JavaServices.IsRunning()) ;
            JavaServices.Process.OutputDataReceived += processresive;
        }

        public void EnableWhiteList(bool enable)
        {
            if (enable)
                SendCommand("whitelist on");
            else
                SendCommand("whitelist off");
        }

        public void DisableTerminalEvent()
        {
            JavaServices.Process.OutputDataReceived -= processresive;
        }

        private void processresive(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null)
            {
                if (e.Data.Contains("Done"))
                {
                    IsDone = true;
                }
                OnTerminal(this, e.Data);
            }
        }

        public void Stop(bool hardstop = false)
        {
            if (!hardstop)
            {
                SendCommand("stop");
                while (JavaServices.IsRunning()) ;
            }

            else
                JavaServices.Stop();

            IsDone = false;
            TerminalEvent = null;


        }

        public async Task StopAsync(bool hardstop = false)
        {
            await Task.Run(() =>
            {
                Stop(hardstop);
            });
        }
    }
}
