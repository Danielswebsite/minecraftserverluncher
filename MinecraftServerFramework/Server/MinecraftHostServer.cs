﻿using MSF.Network;
using MSF.Network.Minecarft;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSF.Network.Packet;
using MSF.Network.Packet.Body;
using MSF.Player;
using MSF.Network.Io;

namespace MSF.Server
{
    /// <summary>
    /// Server for host server
    /// </summary>
    public class MinecraftHostServer : IServer
    {
        /// <summary>
        /// Hostclient there has the server.
        /// </summary>
        private HostClient HostClient;

        public string UUID { get; private set; }

        public string Name { get; private set; }

        public string Port { get; private set; }

        public string Host { get; private set; }

        public string FilePath { get; private set; }
        
        public HostFile Icon { get; set; }

        public MinecraftHostServer(ServerInforPacket serverInforPacket, HostClient hostClient)
        {
            if (serverInforPacket.Body is ServerInfoBody)
            {
                ServerInfoBody infoBody = (ServerInfoBody)serverInforPacket.Body;
                UUID = infoBody.UUID;
                Name = infoBody.Name;
                FilePath = infoBody.FileName;
                Host = infoBody.Host;
                Port = infoBody.Port;                
                this.HostClient = hostClient;
            }
            else
                throw new Exception("The body is not a InfoBody type!");            
        }

        public void EnableTerminalEvent()
        {
            HostClient.ResivePacketEvent += TerminalPacketResive_Event;
        }

        private void TerminalPacketResive_Event(object sender, HostEventArgs e)
        {
            if (e.Packet is TerminalPacket)
            {
                TerminalPacket terminalPacket = (TerminalPacket)e.Packet;

                if (terminalPacket.Body is TerminalBody)
                {
                    TerminalBody terminalBody = (TerminalBody)terminalPacket.Body;

                    OnTerminalEvent(terminalBody.TerminalOutput);
                }
            }
        }

        private void OnTerminalEvent(string line)
        {
            TerminalEvent?.Invoke(this, line);
        }

        public void OpPlayer(MinecraftPlayer player)
        {
            throw new NotImplementedException();
        }

        public void DeOpPlayer(MinecraftPlayer player)
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Stop(bool hardstop = false)
        {
            throw new NotImplementedException();
        }

        public async Task StartAsync()
        {
        
        }

        public async Task StopAsync(bool hardstop = false)
        {
        
        }

        public void SendCommand(string command)
        {
            
        }

        public delegate void TerminalHandler(MinecraftHostServer server, string line);

        public event TerminalHandler TerminalEvent;

    }
}
