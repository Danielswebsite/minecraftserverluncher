﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Server
{
    public class MinecraftServerBackup
    {

        // Minecraft server from construktor
        private MinecraftServer server;

        // Path to save data abute backups
        private string datpath => $"{ Environment.CurrentDirectory }\\backups.dat";

        /// <summary>
        /// Backup a minecraft server and restore minecraft server.
        /// </summary>
        /// <param name="server"></param>
        public MinecraftServerBackup(MinecraftServer server)
        {
            this.server = server;
        }


        /// <summary>
        /// Update data and write paths from zip backups folders from servers.
        /// </summary>
        /// <param name="path"></param>
        private void UpdateDataFile(string path) 
        {
            DirectoryInfo directory = Directory.CreateDirectory(path);

            using (FileStream stream = File.Create(path))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    foreach (var d in directory.GetDirectories())
                    {
                        writer.WriteLine($"{directory.Name}");
                    }
                }
            }
        }

        /// <summary>
        /// Backuo up a minecraft server.
        /// </summary>
        public void Backup(string path)
        {
            ZipFile.CreateFromDirectory(server.ServerPath, $"{path}\\{ server.GetServerFolderName() }_{DateTime.Now.ToString("yyyMMddhhmmss")}.zip");

            
        }

        public void RestoreBackup()
        {

        }

    }
}
