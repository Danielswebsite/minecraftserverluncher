﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Server
{
    public class MinecraftServerEula
    {

        private string path;
        
        public bool AcceptEula { get; set; }

        public MinecraftServerEula(MinecraftServer server)
        {
            path = $"{ server.ServerPath }\\eula.txt";
        }
        
        /// <summary>
        /// Check for is Eula is accepted.
        /// </summary>
        /// <returns></returns>
        public bool IsAccept()
        {
            if (File.Exists(path))
            {
                var lines = File.ReadAllLines(path);
                bool accept = bool.Parse(lines[2].Split('=')[1]);
                return accept;
            }
            else return false;
        }

        private string CreateEulaDate()
        {            
            string dayname = DateTime.Now.ToString("dddd");
            dayname = dayname.First().ToString().ToUpper() + dayname.Substring(1);

            string monthname = DateTime.Now.ToString("MMM");
            monthname = monthname.First().ToString().ToUpper() + monthname.Substring(1);

            return $"{ dayname } { monthname } { DateTime.Now.ToString($"dd HH:mm:ss CET yyyy") }";
        }

        /// <summary>
        /// Save eule. 
        /// </summary>
        public void Save()
        {                       
            StringBuilder builder = new StringBuilder();
            builder.Append($"#By changing the setting below to TRUE you are indicating your agreement to our EULA (https://account.mojang.com/documents/minecraft_eula).{ Environment.NewLine }");
            builder.Append($"#{ CreateEulaDate()}{Environment.NewLine}");
            builder.Append($"eula={ AcceptEula.ToString().ToLower() }{ Environment.NewLine }");            
            File.WriteAllText(path, builder.ToString(), Encoding.ASCII);
        }
    }
}
