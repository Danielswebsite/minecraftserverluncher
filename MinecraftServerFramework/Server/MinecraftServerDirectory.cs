
using System;
using MSF.Tools;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MSF.Server
{
    /// <summary>
    /// Directory for Minecraft servers.
    /// </summary>
    public class MinecraftServerDirectory
    {
        /// <summary>
        /// Path for where the minecraft servere are located on the computer.
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// Make new instace of directory for minecraft servers.
        /// </summary>
        /// <param name="path"></param>
        public MinecraftServerDirectory(string path)
        {
            Path = path;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        /// <summary>
        /// Get all Minecraft servers in directory.
        /// </summary>
        /// <returns></returns>
        public ICollection<MinecraftServer> GetMinecraftServers()
        {

            List<MinecraftServer> servers = new List<MinecraftServer>();

            foreach (var path in Directory.GetDirectories(Path))
            {
                servers.Add(new MinecraftServer() { ServerPath = path, });
            }

            return servers;
        }

        /// <summary>
        /// Delete a server from directory.
        /// </summary>
        /// <param name="name"></param>
        public void DeleteServer(string name)
        {
            Directory.Delete($"{Path}\\{name}", true);
        }

        /// <summary>
        /// Delete a server from directory.
        /// </summary>
        /// <param name="server"></param>
        public void DeleteServer(MinecraftServer server)
        {
            Directory.Delete(server.ServerPath);
        }

        /// <summary>
        /// Create a Minecraft server in directory. 
        /// </summary>
        /// <param name="serverfilepath"></param>
        /// <param name="hasplugin"></param>
        /// <returns></returns>
        public MinecraftServer CreateServer(string serverfilepath, bool hasplugin)
        {
            int i = Directory.GetDirectories(Path).Length + 1;

            string serverpath = $"{Path}\\NewFolder{i}";

            while (Directory.Exists(serverpath))
            {
                i++;
                serverpath = $"{Path}\\NewFolder{i}";
            }

            Directory.CreateDirectory(serverpath);

            if (hasplugin)
                Directory.CreateDirectory($"{ serverpath }\\plugins");

            string name = serverpath.Split('\\').Last();

            File.Copy(serverfilepath, $"{ serverpath }\\{ serverfilepath.Split('\\').Last() }");

            var server = new MinecraftServer();
            server.ServerPath = serverpath;

            return server;
        }
    }
}
