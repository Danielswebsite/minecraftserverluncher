﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Server
{
    public enum GameMode
    {
        Survival = 0,
        Creative = 1,
        Adventure = 2,
        Hardcore = 3,
    }

    public class MinecraftServerProperties
    {       


        public uint SpawnProtection { get; set; }
        public uint MaxTickTime { get; set; }
        public string GeneratorSettings { get; set; }
        public bool ForceGamemode { get; set; }
        public bool AllowNether { get; set; }
        public GameMode Gamemode { get; set; }
        public bool BroadcastConsoleToOps { get; set; }
        public bool EnableQuery { get; set; }
        public uint PlayerIdleTimeout { get; set; }
        public uint Difficulty { get; set; }
        public bool SpawnMonsters { get; set; }
        public byte OpPermissionLevel { get; set; }
        public bool Pvp { get; set; }
        public bool SnooperEnabled { get; set; }
        public string LevelType { get; set; }
        public bool Hardcore { get; set; }
        public bool EnableCommandBlock { get; set; }
        public uint MaxPlayers { get; set; }
        public uint NetworkCompressionThreshold { get; set; }
        public string ResourcePackSha1 { get; set; }
        public uint MaxWorldSize { get; set; }
        public string ServerPort { get; set; }
        public string ServerIp { get; set; }
        public bool SpawnNpcs { get; set; }
        public bool AllowFlight { get; set; }
        public string LevelName { get; set; }
        public uint ViewDistance { get; set; }
        public string ResourcePack { get; set; }
        public bool SpawnAnimals { get; set; }
        public bool WhiteList { get; set; }
        public bool GenerateStructures { get; set; }
        public bool OnlineMode { get; set; }
        public uint MaxBuildHeight { get; set; }
        public long LevelSeed{ get; set; }
        public bool PreventProxyConnections { get; set; }
        public bool EnableRcon { get; set; }
        public string Motd { get; set; }
    }
}
