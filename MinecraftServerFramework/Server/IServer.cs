﻿using MSF.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Server
{
    public interface IServer
    {
        void OpPlayer(MinecraftPlayer player);
        void DeOpPlayer(MinecraftPlayer player);
        void Start();
        void Stop(bool hardstop = false);
        Task StartAsync();
        Task StopAsync(bool hardstop = false);
        void SendCommand(string command);
    }
}