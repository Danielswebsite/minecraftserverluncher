﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Plugin
{
    public class MinecraftPlugin
    {
        public string PluginPath { get; set; }
        public string PluginFolderPath { get; set; }
        public string GetFileName()
        {
            return PluginPath.Split('\\').Last();
        }
    }
}
