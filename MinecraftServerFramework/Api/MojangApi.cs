﻿using MSF.Player;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Api
{
    public class MojangApi
    {
        protected MojangApi()
        {

        }

        public async static Task<MinecraftPlayer> GetPlayerAsync(string name)
        {
            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetStringAsync($"https://api.mojang.com/users/profiles/minecraft/{ name }");
                return JsonConvert.DeserializeObject<MinecraftPlayer>(response.Replace("id", "uuid"));
            }
        }

        public static MinecraftPlayer GetPlayer(string name)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create($"https://api.mojang.com/users/profiles/minecraft/{ name }");
            request.Method = "GET";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    return JsonConvert.DeserializeObject<MinecraftPlayer>(stream.ReadToEnd().Replace("id", "uuid"));
                }
            }

            //using (HttpClient client = new HttpClient())
            //{
            //    var response = client.GetStringAsync($"https://api.mojang.com/users/profiles/minecraft/{ name }").Result;
            //    return JsonConvert.DeserializeObject<MinecraftPlayer>(response.Replace("id", "uuid"));
            //}
        }
    }
}
