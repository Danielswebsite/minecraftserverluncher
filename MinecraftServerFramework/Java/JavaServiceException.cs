﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Java
{
    public class JavaExicuterException : Exception
    {
        public string[] Args { get; set; }
        public string WorkingFolder { get; set; }
    }
}
