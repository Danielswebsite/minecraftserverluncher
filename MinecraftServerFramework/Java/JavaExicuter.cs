﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MSF.Java
{
    public class JavaExicuter
    {

        public Process Process { get; private set; }

        public int Id { get; private set; }

        public string[] Args { get; set; }

        public string WorkingFolder { get; set; }

        public static bool JavaIsInstaled()
        {
            string softwarename = "Java";

            string[] softwareregisterkeys = { @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall" };

            string softwareregisterkey;

            for (int i = 0; i < softwareregisterkeys.Count(); i++)
            {
                softwareregisterkey = softwareregisterkeys[i];

                using (RegistryKey registry = Registry.LocalMachine.OpenSubKey(softwareregisterkey))
                {
                    if (registry != null)
                    {
                        string displayname;

                        foreach (RegistryKey subkey in registry.GetSubKeyNames().Select(k => registry.OpenSubKey(k)))
                        {
                            displayname = subkey.GetValue("DisplayName") as string;

                            if (displayname != null && displayname.Contains(softwarename))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public JavaExicuter()
        {

        }

        public JavaExicuter(int processid)
        {
            try
            {
                Process = Process.GetProcessById(processid);
            }
            catch (Exception e)
            {
                if (e is ArgumentException)
                {
                    Process = null;
                    return;
                }
                throw;
            }
        }

        public void SendText(string text)
        {
            Process.StandardInput.WriteLine(text);
        }

        public bool IsRunning()
        {
            var allprocess = Process.GetProcesses();

            if (Process == null)
                return false;

            return allprocess.ToList().Exists(i => i.Id == Process.Id);
        }

        private string Augumenter()
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < Args.Length; i++)
            {
                if (i != 0)
                    builder.Append($" {Args[i]}");
                else
                    builder.Append($"{Args[i]}");
            }

            return builder.ToString();
        }

        public void Stop()
        {
            try
            {
                Process.Kill();
                Process = null;
            }
            catch (Exception e)
            {
                throw e as JavaExicuterException;
            }
        }

        public async void StartAsync() => await Task.Run(() => { Start(); });

        public void Start()
        {
            string args = Augumenter();

            try
            {

                ProcessStartInfo startprocess = new ProcessStartInfo("java.exe", args);
                startprocess.CreateNoWindow = true;
                startprocess.UseShellExecute = false;
                startprocess.WorkingDirectory = WorkingFolder;
                startprocess.RedirectStandardError = true;
                startprocess.RedirectStandardOutput = true;
                startprocess.RedirectStandardInput = true;

                if ((Process = Process.Start(startprocess)) == null)
                {
                    throw new JavaExicuterException() { Args = Args, WorkingFolder = this.WorkingFolder };
                }

                Process.EnableRaisingEvents = true;
                Process.BeginOutputReadLine();
                Id = Process.Id;
                Process.WaitForExit();
            }

            catch (JavaExicuterException)
            {
                throw;
            }

            finally
            {
                Process = null;
            }
        }
    }
}
