﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSF.Java
{
    public class JavaExicuterSaver
    {
        private static JavaExicuterSaver saver;

        private JavaExicuterSaver()
        {

        }

        public static JavaExicuterSaver GetJavaServicesSaver()
        {
            if (saver == null)
            {
                saver = new JavaExicuterSaver();
            }
            return saver;
        }

        private string path = $"{ Environment.CurrentDirectory }\\data";

        public IEnumerable<JavaExicuter> GetJavaServices()
        {
            if (Directory.Exists(path))
            {
                string[] filenames = Directory.GetFiles(path);

                foreach (var filename in filenames)
                {
                    if (filename.Split(' ').Last().Equals(".dat"))
                    {
                        List<string> lines = File.ReadLines(filename).ToList();

                        foreach (var line in lines)
                        {
                            if (line.Split(' ')[0].Contains("ID"))
                            {
                                yield return new JavaExicuter(int.Parse(line.Split(' ')[1]));
                            }
                        }
                    }
                }
            }
        }

        public void CleanSavedServices(string workingfolder)
        {
            if (Directory.Exists(path))
            {
                Process[] processes = Process.GetProcesses();
                foreach (var filename in Directory.GetFiles(path))
                {
                    foreach (var line in File.ReadAllLines(filename))
                    {
                        if (line.Split(' ')[0].Equals("ID"))
                        {
                            if (!processes.ToList().Exists(i => i.Id == int.Parse(line.Split(' ')[1])))
                            {
                                File.Delete(filename);
                            }
                        }
                    }
                }
            }
        }

        public IEnumerable<JavaExicuter> GetJavaServicesByWorkingFolder(string workingfolder, string[] linesections = null, int id = 0)
        {
            if (Directory.Exists(path))
            {
                foreach (var filename in Directory.GetFiles(path))
                {
                    if (File.Exists(filename))
                    {
                        foreach (var line in File.ReadAllLines(filename))
                        {
                            linesections = line.Split(' ');

                            if (linesections.Length == 2)
                            {
                                if (linesections[0].Equals("ID"))
                                    id = int.Parse(linesections[1]);

                                if (linesections[0].Equals("WorkingFolder"))
                                {
                                    if (linesections[1] == workingfolder)
                                    {
                                        yield return new JavaExicuter(id);
                                    }
                                }
                            }
                        }
                    }
                }
            }


        }

        public void SaveServicesData(JavaExicuter service, StreamWriter streamwriter = null)
        {
            string filename = $"{service.Process.Id}.dat";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            using (streamwriter = File.CreateText($"{path}\\{ filename }"))
            {
                streamwriter.WriteLine($"ID { service.Process.Id }");
                streamwriter.WriteLine($"WorkingFolder { service.WorkingFolder }");
            }
        }

        private string getfilename(int serviceid)
        {
            return $"{serviceid}.dat";
        }

        public void DeleteSavedServicesData(JavaExicuter service)
        {
            string filename = $"{service.Process.Id}.dat";
            File.Delete($"{path}\\{filename}");
        }
    }
}
