﻿using MSF.Java;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MSF.Tools
{
    /// <summary>
    /// Buildtools to download
    /// </summary>
    public class BuildTools
    {
        private static BuildTools buildtools { get; set; }

        /// <summary>
        /// New instace of buildtools with path.
        /// </summary>
        /// <param name="path"></param>
        private BuildTools(string path)
        {
            this.Path = path;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        /// <summary>
        /// Cleare all files and folder unless jar files if thie exsits.
        /// </summary>
        public void ClearBuildTools()
        {
            string[] paths = Directory.GetFiles(Path);

            foreach (string path in paths)
            {
                if (Directory.Exists(path))
                    Directory.Delete(path);
                else if (File.Exists(path))
                    File.Delete(path);
            }

            paths = Directory.GetFiles(Path);

            foreach (string path in paths)
                if (File.Exists(path))
                    if (!path.Contains(".jar"))
                        File.Delete(path);
        }

        /// <summary>
        /// Path for buildtools jar file.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Set buildtools.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static BuildTools SetBuildTools(string path)
        {
            return buildtools = new BuildTools(path);
        }

        /// <summary>
        /// Get exsting instace of buildtools. If buildtools not is set there will be throw a new exception.
        /// </summary>
        /// <returns></returns>
        public static BuildTools GetBuildTools()
        {
            if (buildtools == null)
                throw new Exception("Build tools not set.");

            return buildtools;
        }

        /// <summary>
        /// Check for is BuildTools.jar exsits on path.
        /// </summary>
        /// <returns></returns>
        public bool Exists()
        {
            if (Directory.Exists(Path))
                return File.Exists($"{ Path }\\BuildTools.jar");

            return false;
        }

        /// <summary>
        /// Get all versions of minecraft servers there are in buildtools path.
        /// </summary>
        /// <returns></returns>
        public List<string> GetMinecraftServersVersions()
        {
            var serversversions = new List<string>();

            foreach (var filename in Directory.GetFiles(Path, "*.jar").ToList().FindAll(i => !i.Contains("BuildTools.jar")))
                serversversions.Add(filename.Split('\\').Last());

            return serversversions;
        }

        /// <summary>
        /// Download BuildTools.jar to path from https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
        /// </summary>
        public void DownloadBuildTools()
        {
            using (var webclient = new WebClient())
            {
                webclient.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36");

                string filepath = $"{ Path }\\BuildTools.jar";

                if (!File.Exists(filepath))
                    webclient.DownloadFile("https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar", filepath);
            }
        }

        /// <summary>
        /// Check for is BuildTools.jar is dowloaded and exsits in the path.
        /// </summary>
        /// <returns></returns>
        public bool HasBuildToolsFile()
        {
            return Directory.GetDirectories(Path).ToList().Exists(i => i == "BuildTools.jar");
        }

        /// <summary>
        /// Run BuildTools.jar.
        /// </summary>
        /// <param name="minecrafversion"></param>
        /// <returns></returns>
        public JavaExicuter RunBuildTools(string minecrafversion)
        {
            JavaExicuter services = new JavaExicuter();
            services.Args = new string[] { "-jar", "BuildTools.jar", "--rev", minecrafversion };
            services.WorkingFolder = Path;
            services.StartAsync();
            return services;
        }
    }
}
