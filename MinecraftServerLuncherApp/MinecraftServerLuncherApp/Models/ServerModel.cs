﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinecraftServerLuncherApp.Models
{
    public class ServerModel : Model
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }        
    }
}
