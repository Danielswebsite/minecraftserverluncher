﻿using MinecraftServerLuncherApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace MinecraftServerLuncherApp.ViewModels
{
    public class MainViewModel : ViewModel
    {
        public ObservableCollection<ServerModel> ServerModels { get; set; }

        public MainViewModel(INavigation navigation) : base(navigation) 
        {
            ServerModels = new ObservableCollection<ServerModel>();
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
            ServerModels.Add(new ServerModel { Name = "Server1", Description = "This is a server 1.", Host = "play.danielswebsite.dk", Port = 25565 });
        }
    }
}
