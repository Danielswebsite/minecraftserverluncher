﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MinecraftServerLuncherApp.ViewModels
{
    /// <summary>
    /// ViewModel for busnis logic and other functions.
    /// </summary>
    public abstract class ViewModel
    {
        protected INavigation Naviagtion { get; set; }
        public ViewModel(INavigation navigation) 
        {
            Naviagtion = navigation;
        }

        protected async virtual void NavigatePushAsync(Page page, ViewModel viewModel = null) 
        {
            if (viewModel != null)
                page.BindingContext = viewModel;

            await Naviagtion.PushAsync(page);
        }
    }
}
