﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncherService.Ui
{
    internal class Terminal
    {
        /// <summary>
        /// Car arena for console.
        /// </summary>
        protected static char[,] ConsoleArena { get; set; }

        /// <summary>
        /// Timestamp for terminal output. 
        /// </summary>
        protected static string DateTimeFormatString => $"[{ DateTime.Now.ToString("yyy-MM-dd hh:mm:ss") }]";

        /// <summary>
        /// Commands to hamle actions.
        /// </summary>
        public List<TerminalCommand> Commands { get; protected set; }

        public Terminal()
        {
            Commands = new List<TerminalCommand>();
        }

        /// <summary>
        /// Enable areane to controle a full console layout.
        /// </summary>
        public async void EnableArenaAsync()
        {
            int windowHeight = Console.WindowHeight;
            int windowWidth = Console.WindowWidth;
            ConsoleArena = new char[windowWidth, windowHeight];

            await Task.Run(() =>
            {
                while (true)
                {
                    if (windowHeight != Console.WindowHeight || windowWidth != Console.WindowWidth)
                    {
                        windowHeight = Console.WindowHeight;
                        windowWidth = Console.WindowWidth;
                        ConsoleArena = new char[windowWidth, windowHeight];
                    }
                }
            });
        }

        /// <summary>
        /// Starte command async input listner.
        /// </summary>
        public async void EnableCommandsAsync()
        {
            await Task.Run(() =>
            {
                

                TerminalCommand command;
                while (true)
                {
                    ICollection<string> inputs = Console.ReadLine().Split(' ');
                    string commandName = inputs.FirstOrDefault();
                    inputs.Remove(commandName);
                    string[] commandArgs = inputs.ToArray();

                    command = Commands.FirstOrDefault(i => i.Name.Equals(commandName));

                    if (command is null)
                        PrintLine($"Command '{commandName}' not exsits!");
                    else
                        command.Action(commandArgs);
                }
            });
        }

        /// <summary>
        /// Print text from arene possions.
        /// </summary>
        protected void PrintArena()
        {
            Console.CursorVisible = false;

            char nextPrint;

            for (int x = 0; x < ConsoleArena.GetLength(0); x++)
            {
                for (int y = 0; y < ConsoleArena.GetLength(1); y++)
                {
                    Console.SetCursorPosition(x, y);
                    nextPrint = ConsoleArena[x, y];

                    if (!char.IsWhiteSpace(nextPrint))
                        Console.Write(nextPrint);
                }
            }
        }

        /// <summary>
        /// Read a line from userinputs.
        /// </summary>
        /// <returns></returns>
        public static string ReadLine()
        {
            return Console.ReadLine();
        }

        /// <summary>
        /// Write a linke with curent timespamp.
        /// </summary>
        /// <param name="text"></param>
        public static void PrintLine(string text)
        {
            Console.WriteLine($"{ DateTimeFormatString } { text }");
        }
    }
}
