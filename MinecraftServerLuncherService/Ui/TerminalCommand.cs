﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncherService.Ui
{
    public class TerminalCommand
    {        
        public TerminalCommand(string name, Action<string[]> action) 
        {
            Action = action;
            Name = name;
        }

        /// <summary>
        /// Action to handle with command call.
        /// </summary>
        public Action<string[]> Action { get; set; }

        /// <summary>
        /// Command name.
        /// </summary>
        public string Name { get; set; }

    }
}
