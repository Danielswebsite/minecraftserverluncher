﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncherService.Ui
{
    /// <summary>
    /// Terminal loader.
    /// </summary>
    internal class TerminalLoader : Terminal
    {
        /// <summary>
        /// Get background char for loader.
        /// </summary>
        private readonly char background = '▒';

        /// <summary>
        /// Active loader
        /// </summary>
        private readonly char active = '█';
        
        public int Value { get; set; }

        /// <summary>
        /// Show terminal loader.
        /// </summary>
        public void Show()
        {
            PrintProgressBar();
        }

        private void PrintProgressBar() 
        {
            int buttonIndex = ConsoleArena.GetLength(1) - 1;
            int rigthIndex = ConsoleArena.GetLength(0) - 1;

            int progressBarlength = rigthIndex / 100;

            string loadingString = $" Loader:{ new string(active, Value * progressBarlength) }{ new string(background, (100 - Value) * progressBarlength) } { Value }%";

            for (int x = 0; x < loadingString.Length; x++)
            {
                ConsoleArena[x, buttonIndex] = loadingString[x];
            }
            PrintArena();

        }

        /// <summary>
        /// Hide the terminal loader.
        /// </summary>
        public void Hide() 
        {
            
        }

    }
}
