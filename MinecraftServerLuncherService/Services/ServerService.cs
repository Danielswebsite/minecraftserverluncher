﻿using MinecraftServerLuncherService.Ui;
using MSF.Network;
using MSF.Network.Minecarft;
using MSF.Network.Packet;
using MSF.Network.Packet.Body;
using MSF.Network.Security;
using MSF.Network.Security.Maneger;
using MSF.Network.Security.Resluts;
using MSF.Network.Security.Storeges;
using MSF.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MinecraftServerLuncherService.Services
{
    /// <summary>
    /// Service for hots a Minecraft Host Server.
    /// </summary>
    public class ServerService : ServiceBase
    {
        #region Properties 
        /// <summary>
        /// Host server for 
        /// </summary>
        private HostServer HostServer { get; set; }

        #endregion

        #region Static-methods
        /// <summary>
        /// Get status from abaut the service.
        /// </summary>
        /// <returns></returns>      
        public static ServiceControllerStatus GetStatus()
        {
            ServiceController service = new ServiceController("MinecraftServerLuncher");
            return service.Status;
        }

        /// <summary>
        /// Start the service.
        /// </summary>
        /// <param name="args"></param>
        public static void StartService(string[] args)
        {
            var Service = new ServerService();
            Service.OnStart(args);
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Server start.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            base.OnStart(args);

            Terminal.PrintLine("[INFO] Servivc started");
            Terminal.PrintLine("[INFO] Gettting folder to minecraft servers");
            Terminal.PrintLine("[INFO] Starting host server on ip 127.0.0.1 and port 9855");

            HostServer = new HostServer("127.0.0.1", 9855);

            HostServer.ConnectedEvent += (sender, e) =>
            {
                if (sender is HostConnection)
                {
                    Terminal.PrintLine("[INFO] A new client connected.");
                    HostConnection connection = (HostConnection)sender;
                }
            };

            HostServer.DisconectedEvent += (sender, e) =>
            {
                Terminal.PrintLine("[INFO] A client disconnected.");
            };

            HostServer.ResivePacketEvent += (sender, e) =>
            {
                if (sender is HostConnection)
                {
                    HostConnection connection = (HostConnection)sender;

                    HostAutocation hostAutocation = HostAutocation.GetAutocation(connection);

                    if (hostAutocation.IsPacketTokenAutocatet(e.Packet))
                    {
                        if (e.Packet is AuthPacket)
                            HostServer.SendPacket(connection, new MessagePacket() { Status = 44, Body = new MessageBody() { Message = "Your are already locktin.", Title = "Login Error." } });

                        else
                        {
                            if (e.Packet is ServerInforPacket)
                            {
                                ServerInforPacket serverInforPacket = (ServerInforPacket)e.Packet;
                                ServerInfoBody infoBody = (ServerInfoBody)serverInforPacket.Body;
                            }
                        }
                    }

                    else
                    {
                        if (e.Packet is AuthPacket)
                        {
                            HostUserStoregeProvider<HostUser> storgeprovider = new HostUserStoregeProvider<HostUser>(new FileInfo(@".\Users.xml"));

                            HostUserManager<HostUser> hostUserManager = new HostUserManager<HostUser>(storgeprovider);

                            if (!hostUserManager.HasUsers())
                            {
                                Terminal.PrintLine($"[INFO] No user in storege.");
                                Terminal.PrintLine($"[INFO] Creating new admin user...");
                                hostUserManager.SignUp("admin", "password");
                                Terminal.PrintLine($"[INFO] Done!");
                            }

                            HostLoginManeger<HostUser> loginManeger = new HostLoginManeger<HostUser>(storgeprovider, HostServer, connection);

                            AuthPacket authPacket = (AuthPacket)e.Packet;

                            AuthBody auth = authPacket.Body;

                            LoginResult result = loginManeger.Login(auth.UserName, auth.Password);

                            if (result.IsLogin)
                            {
                                Terminal.PrintLine($"[INFO] User with UUID { result.User.UUID } is sigin to this host with success");
                                MinecraftServerDirectory minecraftServerDirectory = new MinecraftServerDirectory(@"C:\MinecraftServers");
                                ICollection<MinecraftServer> minecraftServers = minecraftServerDirectory.GetMinecraftServers();
                                MinecraftServerHostInformationProvider informationProvider = new MinecraftServerHostInformationProvider(minecraftServers, connection, HostServer, hostAutocation);
                            }

                            else
                            {
                                Terminal.PrintLine($"[INFO] login denied.");
                                HostServer.Disconect(connection);
                            }
                        }
                    }
                }
            };

            Terminal terminal = new Terminal();

            terminal.Commands.Add(new TerminalCommand("add-user", i =>
            {

            }));

            terminal.EnableCommandsAsync();
            HostServer.StartAsync();

            Thread.Sleep(-1);

        }

        /// <summary>
        /// On stop;
        /// </summary>
        protected override void OnStop()
        {
            base.OnStop();
        }
        #endregion
    }
}
