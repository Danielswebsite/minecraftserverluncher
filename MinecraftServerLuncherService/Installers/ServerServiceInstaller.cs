﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncherService.installers
{
    [RunInstaller(true)]
    public partial class ServerServiceInstaller : Installer
    {
        public ServerServiceInstaller()
        {
            Initialze();
        }
    }

    partial class ServerServiceInstaller
    {
        internal static void InstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
        }

        /// <summary>
        /// Check for is service are installed.
        /// </summary>
        /// <returns></returns>
        internal static bool IsInstalled()
        {
            List<ServiceController> services = ServiceController.GetServices().ToList();
            return services.Exists(i => i.ServiceName == "MinecraftServerLuncherService");
        }

        /// <summary>
        /// Uniinstall the service.
        /// </summary>
        internal static void UninstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location, });
        }

        //private IContainer components = null;

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        private ServiceInstaller serviceinstaller;
        private ServiceProcessInstaller serviceprocesinstaller;

        private void Initialze()
        {
            serviceinstaller = new ServiceInstaller();
            serviceprocesinstaller = new ServiceProcessInstaller();

            serviceprocesinstaller.Account = ServiceAccount.LocalSystem;
            serviceprocesinstaller.Username = null;
            serviceprocesinstaller.Password = null;

            serviceinstaller.DisplayName = "Minecraft Server Luncher Service";
            serviceinstaller.ServiceName = "MinecraftServerLuncherService";
            serviceinstaller.Description = "This service host minecraft serveres.";
            serviceinstaller.StartType = ServiceStartMode.Automatic;

            Installers.AddRange(new Installer[] {
                serviceprocesinstaller,
                serviceinstaller
            });
        }
    }

}
