﻿using MinecraftServerLuncherService.installers;
using MinecraftServerLuncherService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftServerLuncherService
{
    static class Program
    {
        [DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                ServiceBase[] services = new ServiceBase[] {
                        new ServerService()
                };
                ServiceBase.Run(services);
            }
            else
            {
                AllocConsole();
                switch (args[0])
                {
                    case "-no-service-console":
                        ServerService.StartService(null);
                        break;
                    case "-install":
                        ServerServiceInstaller.InstallService();
                        break;
                    case "-uninstall":
                        ServerServiceInstaller.UninstallService();
                        break;
                }
            }
        }
    }
}
